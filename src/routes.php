<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

require __DIR__ . '/../src/android/lain_lain/api.php';
require __DIR__ . '/../src/android/master/master_barang.php';
require __DIR__ . '/../src/android/master/master_customer.php';
require __DIR__ . '/../src/android/master/stok.php';
require __DIR__ . '/../src/android/lain_lain/dml.php';
require __DIR__ . '/../src/android/transaksi/keranjang.php';
require __DIR__ . '/../src/android/transaksi/pesanan.php';
require __DIR__ . '/../src/android/lain_lain/images_carousel.php';
require __DIR__ . '/../src/android/transaksi/keranjang_master.php';
require __DIR__ . '/../src/android/setting/setting_sarimbit_master.php';
require __DIR__ . '/../src/android/lain_lain/dml_dump.php';
require __DIR__ . '/../src/android/transaksi/chat_box.php';
require __DIR__ . '/../src/android/lain_lain/histori_approve.php';
require __DIR__ . '/../src/android/master/raja_ongkir.php';
require __DIR__ . '/../src/android/lain_lain/size_pack.php';
require __DIR__ . '/../src/android/lain_lain/versi_app.php';
require __DIR__ . '/../src/android/master/ekspedisi.php';
require __DIR__ . '/../src/android/lain_lain/yjs_api.php';
require __DIR__ . '/../src/android/transaksi/stok.php';

require __DIR__ . '/../src/web/lain_lain/api.php';
require __DIR__ . '/../src/web/master/master_barang.php';
require __DIR__ . '/../src/web/master/master_customer.php';
require __DIR__ . '/../src/web/transaksi/keranjang.php';
require __DIR__ . '/../src/web/transaksi/pesanan.php';
require __DIR__ . '/../src/web/transaksi/keranjang_master.php';
