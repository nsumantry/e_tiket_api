<?php

function get_sql_saldo_barang($barang_seq, $addpar){
    $filter = "";
    if ($barang_seq > 0){
        $filter = " AND b.seq = $barang_seq ";
    }

    $sql =  "SELECT b.seq AS barang_seq, p.stok AS stok ".
            "FROM stok_barang p, master_barang b ".
            "WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter $addpar ".

            " UNION ALL ".

            "SELECT b.seq AS barang_seq, (p.qty - p.terpenuhi - p.qty_tutup) AS stok ".
            "FROM preorder_master pm, preorder_detail p, master_barang b ".
            "WHERE pm.seq = p.master_seq AND b.seq = p.barang_seq AND b.tgl_hapus IS NULL ".
            "AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) >= DATE(pm.berlaku_dari) $filter $addpar ";
    return $sql;
}



function getPlafonTerpakai($db, $customer_seq, $barang_seq, $user_id, $jml_input, $ongkos_kirim, $field_qty){
    $totalKeranjang = get_nilai_keranjang($db, $customer_seq, $user_id, $barang_seq, $jml_input, $field_qty);    

    $sql =  "SELECT plafon, tipe_bayar FROM master_customer WHERE seq = $customer_seq";
    $query = $db->prepare($sql);
    $query->execute();
    $data = $query->fetch();
    
    $plafond    = $data["plafon"];
    $tipe_bayar = $data["tipe_bayar"];    

    //Jika tipe bayarnya deposit ceknya dari penambah pengurang deposit
    $tabel_penambah_pengurang = "";
    if ($tipe_bayar == "P"){
        $tabel_penambah_pengurang = " penambah_pengurang_plafon ";
    }

    $min = "";
    if ($tipe_bayar == "D"){
        $plafond = 0;
        $tabel_penambah_pengurang = " penambah_pengurang_deposit ";
        $min = " - ";
    }
    
    $sql =  "SELECT customer_seq, SUM(jumlah) AS jumlah FROM (".
                "SELECT customer_seq, jumlah AS jumlah ".
                "FROM $tabel_penambah_pengurang ".
                "WHERE customer_seq = $customer_seq ".
                " UNION ALL ".
                "SELECT customer_seq, $min total AS jumlah ".                    
                "FROM pesanan_master ".
                //"WHERE customer_seq = $customer_seq AND status NOT IN ('T','H') AND is_data_lama = 'F' ".
                "WHERE customer_seq = $customer_seq AND status NOT IN ('T','H') ".
                "AND tipe_bayar = '$tipe_bayar' ".
        ") AS hasil GROUP BY customer_seq ";        
    $qry = $db->prepare($sql);  
    $qry->execute(); 
    $rowCount = $qry->rowCount();       
    $terpakai = 0;
    if($rowCount > 0){
        $hasil = $qry->fetch();
        $terpakai  = $hasil["jumlah"];      
    }
    
    if ($tipe_bayar == "P"){
        return ($plafond == 0)||(($totalKeranjang + $terpakai + $ongkos_kirim) <= $plafond);
    }

    if ($tipe_bayar == "D"){
        return (($totalKeranjang + $ongkos_kirim) <= $terpakai);
    }
}



/*
function get_nilai_keranjang($db, $customer_seq, $user_id, $brg_input_seq, $jml_input, $field_qty){  
  
    //sql nyari barang dan qty di keranjang dan disatun dengan barang yang diinput
    $sql =  "SELECT barang_seq, SUM(qty) AS qty FROM ( ".
              "SELECT barang_seq AS barang_seq, $field_qty AS qty ".
              "FROM keranjang WHERE customer_seq = $customer_seq ".
                "UNION ALL ".
              "SELECT DISTINCT $brg_input_seq AS barang_seq, $jml_input  AS qty ".              
            ") AS barang GROUP BY barang_seq ";
    $qry1 = $db->prepare($sql);
    $qry1->execute();
    $array = $qry1->fetchAll();
    //die(var_dump($array));
  
    //Ambil pct diskon di detail customer ------------------------------------------
    $diskon_pct = 0;  
    $sql =  "SELECT pct_diskon_ethica, pct_diskon_seply, pct_diskon_ethica_hijab ".
            "FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id'";
    $qry = $db->prepare($sql); 
    $qry->execute();     

    
    $hasil = $qry->fetch();    
    $pct_diskon_ethica       = $hasil["pct_diskon_ethica"];
    $pct_diskon_seply        = $hasil["pct_diskon_seply"];
    $pct_diskon_ethica_hijab = $hasil["pct_diskon_ethica_hijab"];      
  
    $Total = 0;
    foreach($array as $row) { 
        $barang_seq = $row['barang_seq']; 
        if ($barang_seq > 0) {
            $qty        = $row['qty'];      
    
            //Ambil barang & brand ----------------------------------------------------------
            $sql =  "SELECT b.harga, b.brand_seq, b.sub_brand_seq, d.is_ethica, d.is_seply, d.is_ethicahijab ".
                    "FROM master_barang b, master_brand d ".
                    "WHERE b.brand_seq = d.seq AND b.seq = $barang_seq ";
            $qry = $db->prepare($sql);    
            $qry->execute(); 
            $hasil = $qry->fetch();
            $harga           = $hasil["harga"];
            $is_ethica       = $hasil["is_ethica"];
            $is_seply        = $hasil["is_seply"];
            $is_ethica_hijab = $hasil["is_ethicahijab"];  
            
            //-------------------------------------------------------------------------------
            $disk_ethica = 0;  $disk_seply = 0; $disk_ethica_hijab = 0;
    
            if ($is_ethica == 'T'){
                $disk_ethica = $pct_diskon_ethica;
            }
    
            if ($is_seply == 'T'){
                $disk_seply = $pct_diskon_seply;
            }
    
            if ($is_ethica_hijab == 'T'){
                $disk_ethica_hijab = $pct_diskon_ethica_hijab;
            }    
            
            //cari diskon tertinggi
            $diskon_pct = MAX($disk_ethica, $disk_seply, $disk_ethica_hijab);

            $sql_tgl_release = " AND DATE(b.tgl_release) BETWEEN DATE(s.tgl_dari) AND DATE(s.tgl_sampai) ";
    
            //Cek apakah ada setting diskon atau tidak ------------------------------------
            $sql =  "SELECT range_mulai, range_sampai, diskon_pct ".
                    "FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
                    "WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
                    "AND (".
                            //Untuk diskon berdasarkan klasifikasi             
                            "(b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND b.klasifikasi = s.klasifik) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 
                            "or (b.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = 'Semua') ". // sub brand semua 

                            // //Untuk diskon berdasarkan tgl release
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                    ") AND sm.tgl_hapus IS NULL AND b.seq = $barang_seq  AND b.is_preorder <> 'T' ";

            $qry = $db->prepare($sql); 
            $qry->execute(); 
            $rowCountSetting = $qry->rowCount(); 
            $diskon_pct_setting = 0;
            if ($rowCountSetting > 0){
                $arraySetDiskon = $qry->fetchAll();               
                foreach($arraySetDiskon as $rowSet) { 
                    $dari   = $rowSet['range_mulai'];         
                    $sampai = $rowSet['range_sampai']; 
        
                    if (($qty >= $dari) && ($qty <= $sampai)){
                        $diskon_pct_setting = $rowSet['diskon_pct'];
                    }
                }
            } 
  
            //jika ada diskon dari setting maka yang diambil diskon dari setting
            $diskonAkhir = 0;
            if ($diskon_pct_setting > 0){
                $diskonAkhir = $diskon_pct_setting;
            }else{
                $diskonAkhir = $diskon_pct;
            }
            $harga = $harga - (($harga / 100) * $diskonAkhir);

            $Total += $harga * $qty;
        }
    }   
    return $Total;
  }
function get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, $jml_input, $field_qty, &$from_diskon, &$maxDiskonSetting){  
    $from_diskon = "F";
    $maxDiskonSetting = 0;
    //sql nyari barang dan qty di keranjang dan disatun dengan barang yang diinput
    $sql =  "SELECT barang_seq, SUM(qty_order) AS qty_order FROM ( ".
              "SELECT barang_seq AS barang_seq, $field_qty AS qty_order ".
              "FROM keranjang WHERE customer_seq = $customer_seq AND barang_seq = $brg_input_seq AND user_id = '$user_id' ".
                "UNION ALL ".
              "SELECT DISTINCT $brg_input_seq AS barang_seq, $jml_input AS qty_order ".              
            ") AS barang GROUP BY barang_seq ";            
    $qry1 = $db->prepare($sql);        
    $qry1->execute();
    $array = $qry1->fetchAll();
    //die(var_dump($array));
  
    //Ambil pct diskon di detail customer ------------------------------------------
    $diskon_pct = 0;  
    $sql =  "SELECT pct_diskon_ethica, pct_diskon_seply, pct_diskon_ethica_hijab ".
            "FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id'";
    $qry = $db->prepare($sql); 
    $qry->execute();     
    
    $hasil = $qry->fetch();    
    $pct_diskon_ethica       = $hasil["pct_diskon_ethica"];
    $pct_diskon_seply        = $hasil["pct_diskon_seply"];
    $pct_diskon_ethica_hijab = $hasil["pct_diskon_ethica_hijab"];      
  
    $Total = 0;
    foreach($array as $row) { 
        $barang_seq = $row['barang_seq']; 
        $qty        = $row['qty_order'];
        if (($barang_seq > 0) && ($qty > 0)){            

            //Ambil barang & brand ----------------------------------------------------------
            $sql = "SELECT b.harga, b.brand_seq, b.sub_brand_seq, d.is_ethica, d.is_seply, d.is_ethicahijab ".
                    "FROM master_barang b, master_brand d ".
                    "WHERE b.brand_seq = d.seq AND b.seq = $barang_seq ";
            $qry = $db->prepare($sql);    
            $qry->execute(); 
            $hasil = $qry->fetch();
            $is_ethica       = $hasil["is_ethica"];
            $is_seply        = $hasil["is_seply"];
            $is_ethica_hijab = $hasil["is_ethicahijab"];  
            
            //-------------------------------------------------------------------------------
            $disk_ethica = 0;  $disk_seply = 0; $disk_ethica_hijab = 0;
        
            if ($is_ethica == 'T'){
                $disk_ethica = $pct_diskon_ethica;
            }
        
            if ($is_seply == 'T'){
                $disk_seply = $pct_diskon_seply;
            }
        
            if ($is_ethica_hijab == 'T'){
                $disk_ethica_hijab = $pct_diskon_ethica_hijab;
            }    
            
            //cari diskon tertinggi
            $diskon_pct = MAX($disk_ethica, $disk_seply, $disk_ethica_hijab);
            

            $sql_tgl_release = " AND DATE(b.tgl_release) BETWEEN DATE(s.tgl_dari) AND DATE(s.tgl_sampai) ";

            //Cek apakah ada setting diskon atau tidak ------------------------------------
            $sql =  "SELECT range_mulai, range_sampai, diskon_pct ".
                    "FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
                    "WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
                    "AND ( ".       
                            //Untuk diskon berdasarkan klasifikasi             
                            "(b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND b.klasifikasi = s.klasifik) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 
                            "or (b.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = 'Semua') ". // sub brand semua 

                            // //Untuk diskon berdasarkan tgl release
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                    ") AND sm.tgl_hapus IS NULL AND b.seq = $barang_seq AND b.is_preorder <> 'T' ";

            $qry = $db->prepare($sql); 
            $qry->execute(); 
            $rowCountSetting = $qry->rowCount(); 
            $diskon_pct_setting = 0;
            if ($rowCountSetting > 0){
                $arraySetDiskon = $qry->fetchAll();               
                foreach($arraySetDiskon as $rowSet) { 
                    $dari   = $rowSet['range_mulai'];         
                    $sampai = $rowSet['range_sampai']; 
            
                    if (($qty >= $dari) && ($qty <= $sampai)){
                        $diskon_pct_setting = $rowSet['diskon_pct'];
                        $from_diskon = "T";
                    }
                    $maxDiskonSetting = $diskon_pct_setting;
                }
            } 
    
            //jika ada diskon dari setting maka yang diambil diskon dari setting
            $diskonAkhir = 0;
            if ($diskon_pct_setting > 0){
                $diskonAkhir = $diskon_pct_setting;
            }else{
                $diskonAkhir = $diskon_pct;
            }
            $Total = $diskonAkhir;
        }
    } 
    return $Total; 
}*/


function get_nilai_keranjang($db, $customer_seq, $user_id, $brg_input_seq, $jml_input, $field_qty){  
    //sql nyari barang dan qty di keranjang dan disatun dengan barang yang diinput
    $sql =  "SELECT barang_seq, SUM(qty) AS qty FROM ( ".
              "SELECT barang_seq AS barang_seq, $field_qty AS qty ".
              "FROM keranjang WHERE customer_seq = $customer_seq ".
                "UNION ALL ".
              "SELECT DISTINCT $brg_input_seq AS barang_seq, $jml_input  AS qty ".              
            ") AS barang GROUP BY barang_seq ";
    $qry1 = $db->prepare($sql);
    $qry1->execute();
    $array = $qry1->fetchAll();
    //die(var_dump($array));

    //Ambil pct diskon di detail customer ------------------------------------------
    $diskon_pct = 0;  

    $Total = 0;
    foreach($array as $row) { 
        $barang_seq = $row['barang_seq']; 
        if ($barang_seq > 0) {
            $qty        = $row['qty'];      

            //Ambil barang & brand ----------------------------------------------------------
            $sql = "select max(pct_diskon) as diskon from customer_keagenan_detail c where c.master_seq = $customer_seq ".
                     " and exists(select db.seq from brand_detail db, master_barang b where db.keagenan_seq = c.keagenan_seq and db.master_seq = b.brand_seq and b.seq = $barang_seq)";
            $qry = $db->prepare($sql); 
            $qry->execute();     
            
            $hasil = $qry->fetch();    
            $diskon_pct       = $hasil["diskon"];
            
            $sql =  "SELECT harga FROM master_barang  ".
                    "WHERE seq = $barang_seq ";
            $qry = $db->prepare($sql); 
            $qry->execute(); 
            
            $hasil = $qry->fetch();    
            $harga       = $hasil["harga"];
              
                    
            $sql_tgl_release = " AND DATE(b.tgl_release) BETWEEN DATE(s.tgl_dari) AND DATE(s.tgl_sampai) ";

            //Cek apakah ada setting diskon atau tidak ------------------------------------
            $sql =  "SELECT range_mulai, range_sampai, diskon_pct ".
                    "FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
                    "WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
                    "AND (".
                            //Untuk diskon berdasarkan klasifikasi             
                            "(b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND b.klasifikasi = s.klasifik) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 
                            "or (b.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = 'Semua') ". // sub brand semua 

                            // //Untuk diskon berdasarkan tgl release
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                    ") AND sm.tgl_hapus IS NULL AND b.seq = $barang_seq  AND b.is_preorder <> 'T' ";

            $qry = $db->prepare($sql); 
            $qry->execute(); 
            $rowCountSetting = $qry->rowCount(); 
            $diskon_pct_setting = 0;
            if ($rowCountSetting > 0){
                $arraySetDiskon = $qry->fetchAll();               
                foreach($arraySetDiskon as $rowSet) { 
                    $dari   = $rowSet['range_mulai'];         
                    $sampai = $rowSet['range_sampai']; 
        
                    if (($qty >= $dari) && ($qty <= $sampai)){
                        $diskon_pct_setting = $rowSet['diskon_pct'];
                    }
                }
            } 

            //jika ada diskon dari setting maka yang diambil diskon dari setting
            $diskonAkhir = 0;
            if ($diskon_pct_setting > 0){
                $diskonAkhir = $diskon_pct_setting;
            }else{
                $diskonAkhir = $diskon_pct;
            }  
            $harga = $harga - (($harga / 100) * $diskonAkhir);
            $Total += $harga * $qty;
        }
    }   
    return $Total;
}

function get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, $jml_input, $field_qty, &$from_diskon, &$maxDiskonSetting){  
    $from_diskon = "F";
    $maxDiskonSetting = 0;
    //sql nyari barang dan qty di keranjang dan disatun dengan barang yang diinput
    $sql =  "SELECT barang_seq, SUM(qty_order) AS qty_order FROM ( ".
              "SELECT barang_seq AS barang_seq, $field_qty AS qty_order ".
              "FROM keranjang WHERE customer_seq = $customer_seq AND barang_seq = $brg_input_seq AND user_id = '$user_id' ".
                "UNION ALL ".
              "SELECT DISTINCT $brg_input_seq AS barang_seq, $jml_input AS qty_order ".              
            ") AS barang GROUP BY barang_seq ";            
    $qry1 = $db->prepare($sql);        
    $qry1->execute();
    $array = $qry1->fetchAll();
    //die(var_dump($array));
  

    $Total = 0;
    foreach($array as $row) { 
        $barang_seq = $row['barang_seq']; 
        $qty        = $row['qty_order'];
        if (($barang_seq > 0) && ($qty > 0)){            
        //Ambil pct diskon di detail customer ------------------------------------------

            $sql = "select max(pct_diskon) as diskon from customer_keagenan_detail c where c.master_seq = $customer_seq ".
                     " and exists(select db.seq from brand_detail db, master_barang b where db.keagenan_seq = c.keagenan_seq and db.master_seq = b.brand_seq and b.seq = $barang_seq)";
            $qry = $db->prepare($sql); 
            $qry->execute();     
            
            $hasil = $qry->fetch();    
            $diskon_pct       = $hasil["diskon"];
            

            $sql_tgl_release = " AND DATE(b.tgl_release) BETWEEN DATE(s.tgl_dari) AND DATE(s.tgl_sampai) ";

            //Cek apakah ada setting diskon atau tidak ------------------------------------
            $sql =  "SELECT range_mulai, range_sampai, diskon_pct ".
                    "FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
                    "WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
                    "AND ( ".       
                            //Untuk diskon berdasarkan klasifikasi             
                            "(b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND b.klasifikasi = s.klasifik) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 
                            "or (b.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = 'Semua') ". // sub brand semua 

                            // //Untuk diskon berdasarkan tgl release
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = b.brand_seq AND s.sub_brand_seq = 0 $sql_tgl_release ) ".
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq $sql_tgl_release ) ".
                    ") AND sm.tgl_hapus IS NULL AND b.seq = $barang_seq AND b.is_preorder <> 'T' ";

            $qry = $db->prepare($sql); 
            $qry->execute(); 
            $rowCountSetting = $qry->rowCount(); 
            $diskon_pct_setting = 0;
            if ($rowCountSetting > 0){
                $arraySetDiskon = $qry->fetchAll();               
                foreach($arraySetDiskon as $rowSet) { 
                    $dari   = $rowSet['range_mulai'];         
                    $sampai = $rowSet['range_sampai']; 
            
                    if (($qty >= $dari) && ($qty <= $sampai)){
                        $diskon_pct_setting = $rowSet['diskon_pct'];
                        $from_diskon = "T";
                    }
                    $maxDiskonSetting = $diskon_pct_setting;
                }
            } 
    
            //jika ada diskon dari setting maka yang diambil diskon dari setting
            $diskonAkhir = 0;
            if ($diskon_pct_setting > 0){
                $diskonAkhir = $diskon_pct_setting;
            }else{
                $diskonAkhir = $diskon_pct;
            }
            $Total = $diskonAkhir;
        }
    } 
    return $Total; 
}
  
function getStokBarang($db, $barang_seq, $keranjangseq){                                  
    $SQL_SALDO = get_sql_saldo_barang($barang_seq, "");

    $sqlKeranjang = "";
    if ($keranjangseq > 0){
        $sqlKeranjang = "UNION ALL ".
                        "SELECT barang_seq, qty AS stok ".
                        "FROM keranjang ".
                        "WHERE seq = $keranjangseq AND barang_seq = $barang_seq ";
    };

    $sql =  "SELECT barang_seq, SUM(stok) AS stok FROM (".
                // "SELECT barang_seq, qty AS stok ".
                // "FROM penambah_pengurang_stok WHERE barang_seq = :barang_seq ".
                
                // " UNION ALL ".
                
                // "SELECT barang_seq, -qty AS stok ".                    
                // "FROM pesanan_detail WHERE barang_seq = :barang_seq ".
                // //"AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H', 'S') AND is_data_lama <> 'T')".
                // "AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H'))".
                
                // " UNION ALL ".
                
                // "SELECT barang_seq, -qty AS stok ".                    
                // "FROM keranjang WHERE barang_seq = :barang_seq and seq <> $keranjangseq ".                
                $SQL_SALDO.
                $sqlKeranjang.
        ") AS stoks GROUP BY barang_seq ";          
    $qryCekStok = $db->prepare($sql);  
    //$qryCekStok->bindParam(':barang_seq', $barang_seq);  
    $qryCekStok->execute(); 
    $rowCount = $qryCekStok->rowCount(); 
    
    $stok = 0;
    if ($rowCount > 0) {
      $hasil = $qryCekStok->fetch();
      $stok  = $hasil["stok"];      
    }  
    return $stok;
  }
  
  
// function getStokBarangPreOrder($db, $barang_seq, $keranjangseq){    
//     $sql =  "SELECT barang_seq, SUM(stok) AS stok FROM (".              
//                 "SELECT seq AS barang_seq, jumlah_pesan AS stok ".
//                 "FROM master_barang ".
//                 "WHERE seq = $barang_seq ".
  
//                 " UNION ALL ".
    
//                 "SELECT barang_seq, -qty AS stok ".                    
//                 "FROM pesanan_detail WHERE barang_seq = $barang_seq ".
//                 //"AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H', 'S') AND jenis_so ='P' AND is_data_lama <> 'T') ".
//                 "AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H') AND jenis_so ='P' ) ".
                
//                 " UNION ALL ".
                
//                 "SELECT barang_seq, -qty AS stok ".                    
//                 "FROM keranjang WHERE barang_seq = $barang_seq and seq <> $keranjangseq ".                    
//             ") AS stoks GROUP BY barang_seq ";                  
//     $qryCekStok = $db->prepare($sql);  
//     $qryCekStok->bindParam(':barang_seq', $barang_seq);  
//     $qryCekStok->execute(); 
//     $rowCount = $qryCekStok->rowCount(); 
    
//     $stok = 0;
//     if ($rowCount > 0) {
//       $hasil = $qryCekStok->fetch();
//       $stok  = $hasil["stok"];      
//     }  
//     return $stok;
// }


function getStokBarangPreOrder($db, $barang_seq, $keranjangseq){
    $addPar = " AND exists ( SELECT dm.barang_seq ".
                             "FROM preorder_master pm, preorder_detail dm ".
                             "WHERE pm.seq = dm.master_seq AND dm.barang_seq = b.seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(pm.berlaku_dari) AND DATE(pm.berlaku_sampai) ) ";

    $SQL_SALDO = get_sql_saldo_barang($barang_seq, $addPar);

    $sqlKeranjang = "";
    if ($keranjangseq > 0){
        $sqlKeranjang = "UNION ALL ".
                        "SELECT barang_seq, qty AS stok ".
                        "FROM keranjang ".
                        "WHERE seq = $keranjangseq AND barang_seq = $barang_seq ";
    };

    $sql =  "SELECT barang_seq, SUM(stok) AS stok FROM (".              
                // "SELECT b.seq AS barang_seq, pd.qty - pd.qty_tutup AS stok ".
                // "FROM master_barang b, preorder_detail pd, preorder_master pm ".
                // "WHERE pm.seq = pd.master_seq AND pd.barang_seq = b.seq AND pm.tgl_tutup IS NULL AND b.seq = $barang_seq ".
                // "AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) >= DATE(pm.berlaku_dari) AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) <= DATE(pm.berlaku_sampai) ".
  
                // " UNION ALL ".
    
                // "SELECT b.barang_seq, -b.qty AS stok ".                    
                // "FROM pesanan_detail b, pesanan_master m, preorder_detail pd, preorder_master pm ".
                // "WHERE pm.seq = pd.master_seq AND pd.barang_seq = b.barang_seq AND pm.tgl_tutup IS NULL AND b.barang_seq = $barang_seq ".                
                // "AND m.seq = b.master_Seq AND m.tanggal BETWEEN DATE(pm.berlaku_dari) AND DATE(pm.berlaku_sampai) AND m.status NOT IN ('T','H') AND m.jenis_so ='P' ".                
                
                // " UNION ALL ".
                
                // "SELECT barang_seq, -qty AS stok ".                    
                // "FROM keranjang b WHERE b.barang_seq = $barang_seq and seq <> $keranjangseq ".
                $SQL_SALDO.
                $sqlKeranjang.
            ") AS stoks GROUP BY barang_seq ";            
    $qryCekStok = $db->prepare($sql);      
    $qryCekStok->execute(); 
    $rowCount = $qryCekStok->rowCount(); 
    
    $stok = 0;
    if ($rowCount > 0) {
      $hasil = $qryCekStok->fetch();
      $stok  = $hasil["stok"];      
    }  
    return $stok;
}





// function set_lock_table($db, $nama_table){    
//     $sql =  "SELECT COUNT(*) FROM $nama_table LOCK IN SHARE MODE";
//     $qry = $db->prepare($sql);
//$qry->execute();
// }


function set_lock_table($db, $nama_table){
    $sql =  "LOCK TABLE $nama_table WRITE";
    $qry = $db->prepare($sql);
    $qry->execute();
}


function set_unlock_table($db){
    $sql =  "UNLOCK TABLE";
    $qry = $db->prepare($sql);
    $qry->execute();
}

function wait_lock_table($db, $nama_table){
    $sql =  "SELECT count(*) FROM $nama_table";
    $qry = $db->prepare($sql);
    $qry->execute();
}