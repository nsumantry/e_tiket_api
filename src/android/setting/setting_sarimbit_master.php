<?php
  use Slim\Http\Request;
  use Slim\Http\Response;
  require_once __DIR__ . '/../../../src/global/engine_global.php';

//Untuk mendapatkan daftar sarimbit
$app->get('/setting_sarimbit_master/load', function (Request $request, Response $response, array $argss){
    $search  = $request->getQueryParam("search"); 	
    $offset  = $request->getQueryParam("offset"); 	
    $is_ethica  	 = $request->getQueryParam("is_ethica");
	$is_seply  		 = $request->getQueryParam("is_seply"); 
	$is_ethica_hijab = $request->getQueryParam("is_ethica_hijab");
    $customer_seq    = $request->getQueryParam("customer_seq"); //+new

    $SQL_SALDO = get_sql_saldo_barang(0, " AND b.is_preorder = 'F' ");

    $filter = "";
    if (!empty($search)){
		$filter .= " AND m.nama LIKE '%$search%'";	
    }
    
    $kelompok = "";
	if (!empty($is_ethica)){
		if ($is_ethica == "T"){
			$kelompok .= "'E',";
		}		
	}	

	
	if (!empty($is_seply)){
		if ($is_seply == "T"){
			$kelompok .= "'S',";
		}				
	}	

	
	if (!empty($is_ethica_hijab)){
		if ($is_ethica_hijab == "T"){
			$kelompok .= "'H',";
		}						
	}	
    /*
    if ($kelompok != ""){
        $kelompok = substr($kelompok, 0, -1);
        $filter .= " AND m.kelompok_brand IN ($kelompok)";        
    }
    */
    //+new
    if (!empty($customer_seq)){
        if ($customer_seq != "0"){
            $filter .= " AND exists(select ckg.seq from customer_keagenan_detail ckg where ckg.keagenan_seq = m.keagenan_seq and ckg.master_seq = $customer_seq) ";            
        }                       
    }
    
    // $sql = "SELECT seq, nama, tgl_berlaku_dari, tgl_berlaku_sampai, kelompok_brand, keterangan, user_id, tgl_hapus, gambar, gambar_sedang, gambar_besar ".
    //        "FROM setting_sarimbit_master ".
    //        "WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(tgl_berlaku_dari) AND DATE(tgl_berlaku_sampai) AND tgl_hapus IS NULL $filter ".
    //        "LIMIT 30 OFFSET $offset";

    $sql = "SELECT COUNT(DISTINCT m.seq) AS jumlah_data ".
           "FROM setting_sarimbit_master m, setting_sarimbit_detail d ".
           "WHERE m.seq = d.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(m.tgl_berlaku_dari) AND DATE(m.tgl_berlaku_sampai) AND m.tgl_hapus IS NULL ".
           "AND d.barang_seq IN ( ".
                // "SELECT barang_seq FROM (".
                //     "SELECT barang_seq, qty AS stok ".
                //     "FROM penambah_pengurang_stok ".
                    
                //     " UNION ALL ".
                    
                //     "SELECT barang_seq, -qty AS stok ".                    
                //     "FROM pesanan_detail ".
                //     //"WHERE master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H', 'S') AND is_data_lama <> 'T')".
                //     "WHERE master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H'))".
                    
                //     " UNION ALL ".
                    
                //     "SELECT barang_seq, -qty AS stok ".                    
                //     "FROM keranjang ".

                "SELECT barang_seq FROM (".
                    $SQL_SALDO.
                ") AS stoks GROUP BY barang_seq HAVING SUM(stoks.stok) > 0 ".
           ") $filter ";           
        $query = $this->db->prepare($sql);
        $query->execute();		
        $dtJumlah     = $query->fetch();		
        $jumlah_data  = $dtJumlah["jumlah_data"];

    $sql = "SELECT DISTINCT m.seq, m.nama, m.tgl_berlaku_dari, m.tgl_berlaku_sampai, m.kelompok_brand, m.keterangan, m.user_id, m.tgl_hapus, m.gambar, m.gambar_sedang, m.gambar_besar, $jumlah_data AS jumlah_data ".
           "FROM setting_sarimbit_master m, setting_sarimbit_detail d ".
           "WHERE m.seq = d.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(m.tgl_berlaku_dari) AND DATE(m.tgl_berlaku_sampai) AND m.tgl_hapus IS NULL ".
           "AND d.barang_seq IN ( ".
                // "SELECT barang_seq FROM (".
                //     "SELECT barang_seq, qty AS stok ".
                //     "FROM penambah_pengurang_stok ".
                    
                //     " UNION ALL ".
                    
                //     "SELECT barang_seq, -qty AS stok ".                    
                //     "FROM pesanan_detail ".
                //     //"WHERE master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H', 'S') AND is_data_lama <> 'T')".
                //     "WHERE master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H'))".
                    
                //     " UNION ALL ".
                    
                //     "SELECT barang_seq, -qty AS stok ".                    
                //     "FROM keranjang ".                
                
                "SELECT barang_seq FROM (".
                    $SQL_SALDO.
                ") AS stoks GROUP BY barang_seq HAVING SUM(stoks.stok) > 0 ".
           ")".
           "$filter ORDER BY DATE(m.tgl_berlaku_dari) DESC, m.nama ASC LIMIT 30 OFFSET $offset";           
           
    $query = $this->db->prepare($sql);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {        
            if ($query->rowCount()) {
                $data = $query->fetchAll();
            }else{
                $data = array(['seq' => 0]);
            }       
        }else{
            $data = array([
            'seq' => 0,
            'kode' => 200,
            'keterangan' => 'Tidak ada data',
            'data' => null]);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
    return $response->withJson($data);
  });


$app->get('/setting_sarimbit_master/get/{seq}', function (Request $request, Response $response, array $args){
    $seq  = $args['seq'];    

    $sql =  "SELECT seq, nama, tgl_berlaku_dari, tgl_berlaku_sampai, kelompok_brand, keterangan, user_id, tgl_hapus, gambar, gambar_sedang, gambar_besar ".
            "FROM setting_sarimbit_master ".
            "WHERE seq = $seq ";
    $query = $this->db->prepare($sql);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {        
            if ($query->rowCount()) {
                $data = $query->fetchAll();
            }else{
                $data = array(['seq' => 0]);
            }       
        }else{
            $data = array([
            'seq' => 0,
            'kode' => 200,
            'keterangan' => 'Tidak ada data',
            'data' => null]);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
    return $response->withJson($data);
  });
?>
