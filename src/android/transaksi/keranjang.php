<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../../src/global/engine_global.php';

//Untuk menambahkan barang ke dalam cart
$app->post('/keranjang/save', function (Request $request, Response $response) {
  $dataPost = $request->getParsedBody();
  $db   = $this->db;  
  try {
    $db->beginTransaction();

    $query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){            
            //set_unlock_table($db);
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}
				}
			}
		}    

    if ($dataPost['is_preorder'] == 'T'){      
      //Cek stok masih mencukupi atau tidak  (jumlah preorder yg tersedia)  
      $stok = getStokBarangPreOrder($db, $dataPost['barang_seq'], 0);    
      $stokTerakhir = $stok - $dataPost['qty'];
      if ($dataPost['qty'] > $stok){
        // set_unlock_table($db);    
        return $response->withJson(["status" => "stok kurang", "data" => "1", "stok" => $stok], 200);
      }
    }else{
      //Cek stok masih mencukupi atau tidak    
      $stok = getStokBarang($db, $dataPost['barang_seq'], 0);    
      $stokTerakhir = $stok - $dataPost['qty'];
      if ($dataPost['qty'] > $stok){         
        //set_unlock_table($db);
        return $response->withJson(["status" => "stok kurang", "data" => "1", "stok" => $stok], 200);
      }
    }
    

    //Cek plafon
    if (!getPlafonTerpakai($db, $dataPost['customer_seq'], $dataPost['barang_seq'], $dataPost['user_id'], $dataPost['qty'], 0, "qty")){
      $cust_seq = $dataPost['customer_seq'];
      $sql =  "SELECT tipe_bayar FROM master_customer WHERE seq = $cust_seq";
      $query = $db->prepare($sql);
      $query->execute();
      $data = $query->fetch();
      $tipe_bayar = $data["tipe_bayar"];
      return $response->withJson(["status" => "Plafon tidak mencukupi", "data" => "1", "plafon" => 0, "tipe_bayar" => $tipe_bayar], 200);
    }
    saveMasterKeranjang($db, $dataPost);

    $tipe_apps = $dataPost['tipe_apps'];

    //Cek qty yang sudah tersimpan
    $sql = "SELECT seq FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND user_id = :user_id";
    $querySelect = $db->prepare($sql);
    $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);
    $querySelect->bindParam(':barang_seq', $dataPost['barang_seq']);    
    $querySelect->bindParam(':user_id', $dataPost['user_id']);
    $querySelect->execute(); 
    $rowCount = $querySelect->rowCount(); 

    //Jika ada update jika tidak ada insert baru
    if ($rowCount > 0) {
        $from_setting     = "";
        $maxDiskonSetting = 0;
        $diskon = get_nilai_diskon($db, $dataPost['customer_seq'], $dataPost['user_id'], $dataPost['barang_seq'], $dataPost['qty'], "qty_order", $from_setting, $maxDiskonSetting);        

        $sql = "UPDATE keranjang SET qty = qty + :qty, qty_order = qty_order + :qty, diskon = :diskon ".
               "WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND user_id = :user_id";
        $query = $db->prepare($sql);
        $query->bindParam(':customer_seq', $dataPost['customer_seq']);
        $query->bindParam(':barang_seq', $dataPost['barang_seq']);
        $query->bindParam(':qty', $dataPost['qty']);        
        $query->bindParam(':user_id', $dataPost['user_id']);                
        $query->bindParam(':diskon', $diskon);
        $query->execute();

        $customer_seq_1   = $dataPost['customer_seq'];
        $barang_seq_1     = $dataPost['barang_seq'];
        $qty_1            = $dataPost['qty'];        
        $user_id_1        = $dataPost['user_id'];                

        //Save ke dml
        $sql_1 = "UPDATE keranjang SET qty = qty + $qty_1 ".
                 "WHERE customer_seq = $customer_seq_1 AND barang_seq = $barang_seq_1 AND user_id = '$user_id_1'";

        $sql =  "INSERT INTO dml_dump (sql_1, is_execute) ".
                "VALUES (:sql_1, 'F')";
        $query1 = $db->prepare($sql);       
        $query1->bindParam(':sql_1', $sql_1);        
        $query1->execute();

        $db->commit();
        return $response->withJson(["status" => "success Update", "data" => "1", "stok" => $stokTerakhir], 200);                   
    }else{
        $from_setting     = "";
        $maxDiskonSetting = 0;
        $diskon = get_nilai_diskon($db, $dataPost['customer_seq'], $dataPost['user_id'], $dataPost['barang_seq'], $dataPost['qty'], "qty", $from_setting, $maxDiskonSetting);        
        $sql =  "INSERT INTO keranjang (customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi, qty_order, user_id, is_ceklis, tgl_input, diskon) ".
                "VALUES (:customer_seq, :barang_seq, :qty, :tipe_customer, '$tipe_apps', :qty, :user_id, 'T', NOW(), :diskon)";
        $query = $db->prepare($sql);
        $query->bindParam(':customer_seq', $dataPost['customer_seq']);
        $query->bindParam(':barang_seq', $dataPost['barang_seq']);
        $query->bindParam(':qty', $dataPost['qty']);
        $tipe_customer = "";
        $query->bindParam(':tipe_customer', $tipe_customer);
        $query->bindParam(':user_id', $dataPost['user_id']);
        $query->bindParam(':diskon', $diskon);
        $query->execute();

        //get last seq untuk ke dml
        $id = 0;
        $sql = "SELECT MAX(seq) AS seq, DATE_FORMAT(tgl_input, '%d/%m/%Y %H:%i:%s') as tgl_input FROM keranjang WHERE customer_seq = :customer_seq AND user_id = :user_id AND barang_seq = :barang_seq ";
        $querySelect = $db->prepare($sql);
        $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);          
        $querySelect->bindParam(':user_id', $dataPost['user_id']);  
        $querySelect->bindParam(':barang_seq', $dataPost['barang_seq']);
        $result = $querySelect->execute();
        if ($result) {
          if ($querySelect->rowCount()) {
              $data = $querySelect->fetch();                
              $id = $data["seq"];        
              $tgl_input = $data["tgl_input"];        
          }
        }

        $customer_seq_1  = $dataPost['customer_seq'];
        $barang_seq_1    = $dataPost['barang_seq'];
        $qty_1           = $dataPost['qty'];
        $qty_order_1     = $dataPost['qty'];
        $tipe_customer_1 = "";
        $user_id_1       = $dataPost['user_id'];                       

        //Save ke dml
        $sql_1 =  "INSERT INTO keranjang (seq, customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi, qty_order, user_id, is_ceklis, tgl_input) ".
                  "VALUES ($id, $customer_seq_1, $barang_seq_1, $qty_1, '$tipe_customer_1', '$tipe_apps', $qty_order_1, '$user_id_1', 'T', TO_TIMESTAMP('$tgl_input', 'DD/MM/YYYY HH24:MI:SS'))";

        $sql =  "INSERT INTO dml_dump (sql_1, is_execute) ".
                "VALUES (:sql_1, 'F')";
        $query1  = $db->prepare($sql);     
        $query1->bindParam(':sql_1', $sql_1);      
        $query1->execute();
        
        $db->commit();        
        //set_unlock_table($db);
        return $response->withJson(["status" => "success Save", "data" => $rowCount, "stok" => $stokTerakhir], 200);       
    }     

    // $sql = "COMMIT";
    // $query = $db->prepare($sql);        
    // $query->execute();

    
  } catch(PDOException $pdoe) {
    $db->rollBack();
    //set_unlock_table($db);
    throw $pdoe;
    return $response->withJson(["status" => "gagal"], 100);  
  }catch(Exception $e) {  
    $db->rollBack();
    //set_unlock_table($db);
    return $response->withJson(["status" => "gagal"], 100);  
  } 
})->add($cekAPIKey);


//Untuk menghapus barang dari cart
$app->get("/keranjang/delete/{id}", function (Request $request, Response $response, $args){
  $db   = $this->db;
  try {
    $db->beginTransaction();    

    $query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}
				}
			}
		}

    $id = $args["id"];

    $sql = "SELECT customer_seq, user_id, 0 AS ongkos_kirim FROM keranjang WHERE seq = $id";    
    $querySelect = $db->prepare($sql);
    $result = $querySelect->execute();

    $user_id      = "";
    $customer_seq = "";
    if ($result) {
      if ($querySelect->rowCount()) {
          $data         = $querySelect->fetch();
          $customer_seq = $data["customer_seq"];  
          $user_id      = $data["user_id"];  
      }
    }    

    $sql = "DELETE FROM keranjang WHERE seq =:id";
    $query = $db->prepare($sql);
    $query->bindParam(':id', $args['id']);
    $result = $query->execute();

    //Save ke dml
    $sql_1 =  "DELETE FROM keranjang WHERE seq = $id";                  

    $sql =  "INSERT INTO dml_dump (sql_1, is_execute) ".
            "VALUES (:sql, 'F')";
    $query1  = $db->prepare($sql);
    $query1->bindParam(':sql', $sql_1);                
    $result1 = $query1->execute();


    $jml_keranjang = 0;
    $sql = "SELECT COUNT(*) AS jumlah FROM keranjang WHERE user_id = '$user_id' AND customer_seq = $customer_seq ";
    $qry = $db->prepare($sql);
    $result = $qry->execute();
    if ($result) {      
      $hasil = $qry->fetch();          
      $jml_keranjang = $hasil["jumlah"];      
      if ($jml_keranjang == 0){        
        $sql = "DELETE FROM keranjang_master WHERE user_id = '$user_id' AND customer_seq = $customer_seq ";
        $query = $db->prepare($sql);        
        $query->execute();

        saveMasterKeranjang($db, $data);
      }      
    }
    
    if($result1){
      $db->commit();
      return $response->withJson(["status" => "success", "data" => "1"], 200);
    }
    $db->rollBack();
  } catch(PDOException $pdoe) {
    $db->rollBack();
    throw $pdoe;
    return $response->withJson(["status" => "gagal"], 100);  
  }catch(Exception $e) {  
    $db->rollBack();
    return $response->withJson(["status" => "gagal"], 100);  
  } 
  return $response->withJson(["status" => "failed", "data" => "0"], 200);
})->add($cekAPIKey);
  

//Untuk mendapatkan daftar cart
$app->get('/keranjang/load/{id}', function (Request $request, Response $response, array $args) {  
  $filter     = "";
  //$tipe_cust  = $request->getQueryParam("tipe_cust");
  $user_id    = $request->getQueryParam("user_id"); //+new
  
  //$fielddiskon = "(c.diskon_ethica * b.harga)/100";
  // if (!empty($tipe_cust)){
  //   if ($tipe_cust == "E"){ 
  //     //$filter .= " AND b.brand <> 'SEPLY' AND k.tipe_customer = '$tipe_cust' ";
  //     //$filter .= " AND k.tipe_customer = '$tipe_cust'";      
  //   }
  //   if ($tipe_cust == "S"){ 
  //     //$filter .= " AND b.brand in ('SEPLY', 'OHYA', 'MAJORI') AND k.tipe_customer = '$tipe_cust' ";
  //     //$filter .= " AND k.tipe_customer = '$tipe_cust'";
  //     $fielddiskon = "(c.diskon_seply * b.harga)/100";
  //   }
  // }

  $sql =  "SELECT b.seq as brgseq, b.barcode, b.nama as nama, b.barcode as kode, b.harga as harga, b.gambar, k.seq as seq, k.barang_seq as barang_seq, ".
          "k.qty as qty, k.customer_seq as customer_seq, IFNULL((b.harga - ((b.harga / 100) * k.diskon)),0) as diskon, k.tgl_input, k.qty_order, b.berat * k.qty_order as berat_total, b.berat, b.is_preorder ".
          "FROM master_barang b, keranjang k, master_customer c ".
          "WHERE c.seq = k.customer_seq and k.barang_seq = b.seq and k.qty > 0 and k.customer_seq = :id  AND k.user_id = '$user_id' $filter";          
  $query = $this->db->prepare($sql);
  $query->bindParam(':id', $args['id']);
  $result = $query->execute();
  if ($result) {
    if ($query->rowCount()) {
      $data = $query->fetchAll();
    }else{
      $data = array(
        'kode' => 200,
        'keterangan' => 'Tidak ada data',
        'data' => null);
    }
  }else{
    $data = array(
      'kode' => 100,
      'keterangan' => 'Terdapat error',
      'data' => null);
  }
    return $response->withJson($data);
});


//Untuk mendapatkan jumlah barang yang ada di cart
$app->get('/keranjang/getcount/{id}', function (Request $request, Response $response, array $args) {
  $filter = "";
  $user_id    = $request->getQueryParam("user_id"); //+new

  $sql =  "SELECT count(*) as total FROM master_barang b, keranjang k, master_customer c ".
          "WHERE c.seq = k.customer_seq and k.barang_seq = b.seq and k.qty > 0 and k.customer_seq = :id and k.user_id = '$user_id' $filter";          
  $query = $this->db->prepare($sql);  
  $query->bindParam(':id', $args['id']);
  $result = $query->execute();
  if ($result) {
    if ($query->rowCount()) {
      $data = $query->fetchAll();
    }else{
      $data = array(
        'kode' => 200,
        'keterangan' => 'Tidak ada data',
        'data' => null);
    }
  }else{
    $data = array(
      'kode' => 100,
      'keterangan' => 'Terdapat error',
      'data' => null);
  }
    return $response->withJson($data);
});



// $app->get('/keranjang/getDiskon/', function (Request $request, Response $response, array $args) {  
//   $filter = "";
//   $from_setting = "F";

//   $db   = $this->db;
//   $customer_seq  = $request->getQueryParam("customer_seq");  
//   $user_id       = $request->getQueryParam("user_id"); 
//   $barang_seq    = $request->getQueryParam("barang_seq");    
//   $qty = $request->getQueryParam("qty");

//   $sql =  "SELECT seq, barang_seq, qty ".
//           "FROM keranjang WHERE customer_seq = $customer_seq and user_id = '$user_id' and barang_seq = '$barang_seq' ";
//   $qry1 = $db->prepare($sql);
//   $qry1->execute();
//   $array = $qry1->fetchAll();

//   $diskon = 0;
//   foreach($array as $row) { 
//     //Ambil barang & brand ----------------------------------------------------------
//     $sql = "SELECT b.harga, b.brand_seq, b.sub_brand_seq, d.is_ethica, d.is_seply, d.is_ethicahijab ".
//            "FROM master_barang b, master_brand d ".
//            "WHERE b.brand_seq = d.seq AND b.seq = $barang_seq ";
//     $qry = $db->prepare($sql);    
//     $qry->execute(); 
//     $hasil = $qry->fetch();
//     $harga           = $hasil["harga"];   
//     $brand_seq       = $hasil["brand_seq"];   
//     $sub_brand_seq   = $hasil["sub_brand_seq"];       
//     $is_ethica       = $hasil["is_ethica"];
//     $is_seply        = $hasil["is_seply"];
//     $is_ethica_hijab = $hasil["is_ethicahijab"];  
    
//     //Cek apakah ada setting diskon atau tidak ------------------------------------
//     $sql = "SELECT diskon_pct FROM setting_diskon_detail ".
//            "WHERE master_seq IN ( SELECT seq FROM setting_diskon_master ".
//                                 " WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(tgl_berlaku_dari) AND DATE(tgl_berlaku_sampai) ".
//                                 " AND tgl_hapus IS NULL) ".
//           "AND brand_seq = $brand_seq AND sub_brand_seq = $sub_brand_seq AND ".$qty." between range_mulai, range_sampai ";

//     $qry = $db->prepare($sql); 
//     $qry->execute(); 
//     $rowCountSetting = $qry->rowCount(); 
//     $diskon = 0;
//     if ($rowCountSetting > 0){//jika ada diskon ambil dari setting diskon
//       $arraySetDiskon = $qry->fetchAll();               
//       foreach($arraySetDiskon as $rowSet) { 
//           $diskon = $rowSet['diskon_pct'];
//       }
//     }else {//jika tidak ada ambil dari master customer 

//       //Ambil pct diskon di detail customer ------------------------------------------
//       $sql =  "SELECT pct_diskon_ethica, pct_diskon_seply, pct_diskon_ethica_hijab ".
//               "FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id'";
//       $qry = $db->prepare($sql); 
//       $qry->execute();     
      
//       $hasil = $qry->fetch();    
//       $pct_diskon_ethica       = $hasil["pct_diskon_ethica"];
//       $pct_diskon_seply        = $hasil["pct_diskon_seply"];
//       $pct_diskon_ethica_hijab = $hasil["pct_diskon_ethica_hijab"];  


//       $disk_ethica = 0;  
//       $disk_seply = 0; 
//       $disk_ethica_hijab = 0;

//       if ($is_ethica == 'T'){
//         $disk_ethica = $pct_diskon_ethica;
//       }

//       if ($is_seply == 'T'){
//         $disk_seply = $pct_diskon_seply;
//       }

//       if ($is_ethica_hijab == 'T'){
//         $disk_ethica_hijab = $pct_diskon_ethica_hijab;
//       }    
      
//       //cari diskon tertinggi
//       $diskon = MAX($disk_ethica, $disk_seply, $disk_ethica_hijab);
//     }
//   } 
//   $data = array(
//     'kode' => 100,
//     'diskon' => $diskon,
//     'from_setting' => $from_setting,);
//     return $response->withJson($data);
// })->add($cekAPIKey);


//Untuk menambah atau mengurang jumlah barang di cart
$app->post('/keranjang/update_qty', function (Request $request, Response $response) {
  $dataPost = $request->getParsedBody();
  $db   = $this->db;
  
  try {
    $db->beginTransaction();            
    $customer_seq  = $dataPost['customer_seq']; 
    $user_id       = $dataPost['user_id'];
    $brg_input_seq = $dataPost['barang_seq'];

    $query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}
				}
			}
		}

    if ($dataPost['tipe'] == "T"){    

        if ($dataPost['is_preorder'] == 'T'){      
          //Cek stok masih mencukupi atau tidak  (jumlah preorder yg tersedia)  
          $stok = getStokBarangPreOrder($db, $dataPost['barang_seq'], $dataPost['seq']);
          if ($dataPost['qty'] > $stok){   
            $db->rollBack();
            return $response->withJson(["status" => "stok kurang", "data" => "1", "stok" => $stok], 200);
          }
        }else{          
          //Cek stok masih mencukupi atau tidak    
          $stok = getStokBarang($db, $dataPost['barang_seq'], $dataPost['seq']);    
          if ($dataPost['qty'] > $stok){   
            $db->rollBack();
            return $response->withJson(["status" => "stok kurang", "data" => "1", "stok" => $stok], 200);
          }
        }

        //Cek plafon
        if (!getPlafonTerpakai($db, $dataPost['customer_seq'], $dataPost['barang_seq'], $dataPost['user_id'], 1, 0, "qty")){
          $cust_seq = $dataPost['customer_seq'];
          $sql =  "SELECT tipe_bayar FROM master_customer WHERE seq = $cust_seq";
          $query = $db->prepare($sql);
          $query->execute();
          $data = $query->fetch();
          $tipe_bayar = $data["tipe_bayar"];          
          return $response->withJson(["status" => "Plafon tidak mencukupi", "data" => "1", "plafon" => 0, "tipe_bayar" => $tipe_bayar], 200);
        }
        
        $sql = "UPDATE keranjang SET qty = :qty WHERE seq = :seq";
        $query = $db->prepare($sql);
        $query->bindParam(':seq', $dataPost['seq']);
        $query->bindParam(':qty', $dataPost['qty']);
        $result = $query->execute();

        $seq_1  = $dataPost['seq'];              
        $qty_1  = $dataPost['qty'];
        
        //Save ke dml
        $sql_1 = "UPDATE keranjang SET qty = $qty_1 WHERE seq = $seq_1";
        $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                "VALUES (:sql_1, 'F')";
        $query1  = $db->prepare($sql);     
        $query1->bindParam(':sql_1', $sql_1);         
        $result1 = $query1->execute();

        if($result && $result1){
          $db->commit();
          return $response->withJson(["status" => "success", "data" => "1"], 200);  
        }                          
    }else{
      $qty_order_keranjang = 0;        
      $seq_keranjang = $dataPost['seq'];
      $sql = "SELECT qty_order FROM keranjang WHERE seq = $seq_keranjang ";      
      $querySelect = $db->prepare($sql);
      $result = $querySelect->execute();
      if ($result) {
        if ($querySelect->rowCount()) {
            $data = $querySelect->fetch();            
            $qty_order_keranjang = $data["qty_order"];        
        }
      }
      
      //juka mengurangi qty order hitung ulang diskonnya
      $selisih = 0;
      if ($dataPost['qty'] == $dataPost['qty_order']){
        $selisih = $dataPost['qty'] - $qty_order_keranjang;
      }            

      $diskon =  get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, $selisih, "qty_order", $from_setting, $maxDiskonSetting);      
      $query = $db->prepare('UPDATE keranjang SET qty = :qty, qty_order = :qty_order, diskon = :diskon WHERE seq = :seq');
      $query->bindParam(':seq', $dataPost['seq']);
      $query->bindParam(':qty', $dataPost['qty']);
      $query->bindParam(':qty_order', $dataPost['qty_order']);
      $query->bindParam(':diskon', $diskon);
      $result = $query->execute();      

      $seq_1  = $dataPost['seq'];              
      $qty_1  = $dataPost['qty'];
      $qty_order  = $dataPost['qty_order'];
      
      //Save ke dml
      $sql_1 = "UPDATE keranjang SET qty = $qty_1, qty_order = $qty_order WHERE seq = $seq_1";
      $sql =  "INSERT INTO dml_dump (sql_1, is_execute) ".
                "VALUES (:sql_1, 'F')";
      $query1  = $db->prepare($sql);     
      $query1->bindParam(':sql_1', $sql_1);     
      $result1 = $query1->execute();

      if($result && $result1){
        $db->commit();
        return $response->withJson(["status" => "success", "data" => "1", "diskon" => $diskon, "dari_setting" => $from_setting, "max_diskon_setting" => $maxDiskonSetting], 200);  
      }          
    }
  } catch(PDOException $pdoe) {
    $db->rollBack();
    throw $pdoe;
    return $response->withJson(["status" => "gagal"], 100);  
  }catch(Exception $e) {  
    $db->rollBack();
    return $response->withJson(["status" => "gagal"], 100);  
  }   
})->add($cekAPIKey);

//Untuk menambah atau mengurang jumlah barang di cart yang akan di keep
$app->post('/keranjang/update_qty_order', function (Request $request, Response $response) {
  $dataPost = $request->getParsedBody();
  $db   = $this->db;
  $from_setting = "";
  $maxDiskonSetting = 0;
  try {
    $db->beginTransaction();    
      $customer_seq = $dataPost['customer_seq']; 
      $user_id = $dataPost['user_id'];
      $brg_input_seq = $dataPost['barang_seq'];
      if ($dataPost['tipe'] == "T"){    
        $diskon = get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, 1, "qty_order", $from_setting, $maxDiskonSetting);
        $sql = "UPDATE keranjang SET qty_order = :qty_order, diskon = :diskon WHERE seq = :seq";
        $query = $db->prepare($sql);
        $query->bindParam(':seq', $dataPost['seq']);
        $query->bindParam(':qty_order', $dataPost['qty_order']);
        $query->bindParam(':diskon', $diskon);
        $result = $query->execute();

        if($result){
          $db->commit();
          return $response->withJson(["status" => "success", "data" => "1", "diskon" => $diskon, "dari_setting" => $from_setting, "max_diskon_setting" => $maxDiskonSetting], 200);
        }                          
    }else{
      $diskon =  get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, -1, "qty_order", $from_setting, $maxDiskonSetting);
      $query = $db->prepare('UPDATE keranjang SET qty_order = :qty_order, diskon = :diskon WHERE seq = :seq');
      $query->bindParam(':seq', $dataPost['seq']);
      $query->bindParam(':qty_order', $dataPost['qty_order']);
      $query->bindParam(':diskon', $diskon);
      $result = $query->execute();

      if($result){
        $db->commit();
        return $response->withJson(["status" => "success", "data" => "1", "diskon" => $diskon, "dari_setting" => $from_setting, "max_diskon_setting" => $maxDiskonSetting], 200);  
      }          
    }
  } catch(PDOException $pdoe) {
    $db->rollBack();
    throw $pdoe;
    return $response->withJson(["status" => "gagal"], 100);  
  }catch(Exception $e) {  
    $db->rollBack();
    return $response->withJson(["status" => "gagal"], 100);  
  }   
});

//Unuk mendapatkan nilai diskon barang yang ada di cart
$app->post('/keranjang/get_nilai_diskon', function (Request $request, Response $response) {
  $dataPost = $request->getParsedBody();
  $db       = $this->db;
  $from_setting      = "";
  $maxDiskonSetting  = 0;
  $customer_seq  = $dataPost['customer_seq']; 
  $user_id       = $dataPost['user_id'];
  $brg_input_seq = $dataPost['barang_seq'];
  $diskon        = get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, 0, "qty_order", $from_setting, $maxDiskonSetting);
  return $response->withJson(["status" => "success", "data" => "1", "diskon" => $diskon, "dari_setting" => $from_setting, "max_diskon_setting" => $maxDiskonSetting], 200);  
});


$app->get('/keranjang/auto_delete', function (Request $request, Response $response, array $args) {      
  $db   = $this->db;
  try {       
    $db->beginTransaction();    

    //delete per 24 jam
    $seqKeranjang = 0;
    $sql = "SELECT seq FROM keranjang WHERE tgl_input <= NOW() - INTERVAL 1 DAY AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'E')";
    $querySelect = $db->prepare($sql);
    $result = $querySelect->execute(); 
    if ($result) {
      if ($querySelect->rowCount()) {
        $array = $querySelect->fetchAll();        
        foreach($array as $row) {      
          $seqKeranjang = $row['seq'];            

          $sql_1 = "DELETE FROM keranjang WHERE seq = $seqKeranjang";          
          $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                   "VALUES ('$sql_1', 'F')";                                     
          $query1 = $db->prepare($sql);                
          $query1->execute();
        }   

        $sql = "DELETE FROM keranjang WHERE tgl_input <= NOW() - INTERVAL 1 DAY AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'E')";
        $query = $db->prepare($sql);
        $query->execute();  
      }
    }      

    //Delete per jam 8
    $seqKeranjang = 0;
    $sql = "SELECT seq FROM keranjang WHERE DATE_FORMAT(tgl_input,'%y-%m-%d') < DATE_FORMAT(NOW(),'%y-%m-%d') AND TIME(NOW()) > STR_TO_DATE('08:00:00', '%H:%i:%s')".
           "AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'D')";
    $querySelect = $db->prepare($sql);
    $result = $querySelect->execute(); 
    if ($result) {
      if ($querySelect->rowCount()) {
        $array = $querySelect->fetchAll();        
        foreach($array as $row) {      
          $seqKeranjang = $row['seq'];            

          $sql_1 = "DELETE FROM keranjang WHERE seq = $seqKeranjang";          
          $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                   "VALUES ('$sql_1', 'F')";                                     
          $query1 = $db->prepare($sql);                
          $query1->execute();
        }   

        $sql = "DELETE FROM keranjang WHERE DATE_FORMAT(tgl_input,'%y-%m-%d') < DATE_FORMAT(NOW(),'%y-%m-%d') AND TIME(NOW()) > STR_TO_DATE('08:00:00', '%H:%i:%s')".
               "AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'D')";
        $query = $db->prepare($sql);
        $query->execute();  
      }
    }  


    //delete per 24 jam
    $SeqPesanan = 0;
    $sql = "SELECT seq FROM pesanan_master WHERE tgl_input <= NOW() - INTERVAL 1 DAY AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'E') and status = 'K' ";
    $querySelect = $db->prepare($sql);
    $result = $querySelect->execute(); 
    if ($result) {
      if ($querySelect->rowCount()) {
        $array = $querySelect->fetchAll();        
        foreach($array as $row) {      
          $SeqPesanan = $row['seq'];            

          $sql_1 = "DELETE FROM pesanan_detail WHERE master_seq = $SeqPesanan";
          $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                   "VALUES ('$sql_1', 'F')";                                     
          $query1 = $db->prepare($sql);                
          $query1->execute();

          $sql_1 = "DELETE FROM pesanan_master WHERE seq = $SeqPesanan";
          $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                   "VALUES ('$sql_1', 'F')";                                     
          $query1 = $db->prepare($sql);                
          $query1->execute();          
        }   

        $sql = "DELETE FROM pesanan_detail WHERE master_seq in(select seq from pesanan_master where status = 'K' AND tgl_input <= NOW() - INTERVAL 1 DAY AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'E'))";
        $query = $db->prepare($sql);
        $query->execute();  

        $sql = "DELETE FROM pesanan_master WHERE status = 'K' AND tgl_input <= NOW() - INTERVAL 1 DAY AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'E')";
        $query = $db->prepare($sql);
        $query->execute();  
      }
    }      

    //Delete per jam 8
    $SeqPesanan = 0;
    $sql = "SELECT seq FROM pesanan_master WHERE DATE_FORMAT(tgl_input,'%y-%m-%d') < DATE_FORMAT(NOW(),'%y-%m-%d') AND TIME(NOW()) > STR_TO_DATE('08:00:00', '%H:%i:%s')".
           "AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'D') and status = 'K' ";
    $querySelect = $db->prepare($sql);
    $result = $querySelect->execute(); 
    if ($result) {
      if ($querySelect->rowCount()) {
        $array = $querySelect->fetchAll();        
        foreach($array as $row) {      
          $SeqPesanan = $row['seq'];

          $sql_1 = "DELETE FROM pesanan_detail WHERE master_seq = $SeqPesanan";
          $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                   "VALUES ('$sql_1', 'F')";                                     
          $query1 = $db->prepare($sql);                
          $query1->execute();

          $sql_1 = "DELETE FROM pesanan_master WHERE seq = $SeqPesanan";
          $sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
                   "VALUES ('$sql_1', 'F')";                                     
          $query1 = $db->prepare($sql);                
          $query1->execute();          
        }   

        $sql = "DELETE FROM pesanan_detail WHERE master_seq in(select seq from pesanan_master WHERE status = 'K' AND DATE_FORMAT(tgl_input,'%y-%m-%d') < DATE_FORMAT(NOW(),'%y-%m-%d') AND TIME(NOW()) > STR_TO_DATE('08:00:00', '%H:%i:%s')".
               "AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'D'))";
        $query = $db->prepare($sql);
        $query->execute();  

        $sql = "DELETE FROM pesanan_master WHERE status = 'K' AND DATE_FORMAT(tgl_input,'%y-%m-%d') < DATE_FORMAT(NOW(),'%y-%m-%d') AND TIME(NOW()) > STR_TO_DATE('08:00:00', '%H:%i:%s')".
               "AND customer_seq IN ( SELECT seq FROM master_customer WHERE tipe_hapus = 'D')";
        $query = $db->prepare($sql);
        $query->execute();  
      }
    }  

    $db->commit();
  } catch(PDOException $pdoe) {
    $db->rollBack();
    throw $pdoe;
    return $response->withJson(["status" => "gagal"], 100);  
  }catch(Exception $e) {  
    $db->rollBack();
    return $response->withJson(["status" => "gagal"], 100);  
  }  
  return $response->withJson(["status" => "success"], 200);       
});


function saveMasterKeranjang($db, $dataPost){   

    //Save ke alamat jika belum ada di keranjang
   $CountKeranjang = 0;
   $sql = "SELECT count(*) AS jml FROM keranjang_master WHERE customer_seq = :customer_seq AND user_id = :user_id";
   $querySelect = $db->prepare($sql);
   $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);      
   $querySelect->bindParam(':user_id', $dataPost['user_id']);  //+new
   $result = $querySelect->execute(); 
   if ($result) {
     if ($querySelect->rowCount()) {
         $data = $querySelect->fetch();        
         $CountKeranjang = $data["jml"];
     }
   }  

   $tipe_apps = $dataPost['tipe_apps'];

   if ($CountKeranjang == 0) {  
        $sql = "DELETE FROM keranjang_master WHERE customer_seq = :customer_seq AND user_id = :user_id";
        $query = $db->prepare($sql);
        $query->bindParam(':customer_seq', $dataPost['customer_seq']);        
        $query->bindParam(':user_id', $dataPost['user_id']);  //+new
        $query->execute(); 

        $alamat_kirim = "";  
        $sql =  "SELECT nama, alamat, kota, provinsi, no_telp, id_provinsi, id_kota, id_kecamatan, kelurahan, kecamatan ".
                "FROM master_customer WHERE seq = :customer_seq ";
        $query = $db->prepare($sql);    
        $query->bindParam(':customer_seq', $dataPost['customer_seq']);
        $result = $query->execute();

        $nama_kirim           = "";
        $id_provinsi          = 0;
        $id_kota              = 0;
        $id_kecamatan         = 0;
        $provinsi             = "";
        $kota                 = "";
        $kecamatan            = "";
        $kelurahan            = "";
        $alamat_kirim_lengkap = "";
        $no_telepon_kirim     = "";
    
        if ($result) {
            if ($query->rowCount()) {
                $data = $query->fetch();               

                $nama_kirim           = $data["nama"];
                $id_provinsi          = $data["id_provinsi"];
                $id_kota              = $data["id_kota"];
                $id_kecamatan         = $data["id_kecamatan"];
                $provinsi             = $data["provinsi"];
                $kota                 = $data["kota"];
                $kecamatan            = $data["kecamatan"];
                $kelurahan            = $data["kelurahan"];                
                $no_telepon_kirim     = $data["no_telp"];
                $alamat_kirim_lengkap = $data["alamat"];
            
                $alamatTmp = "";
                if ($data["alamat"] <> ""){
                    $alamatTmp .= $data["alamat"].", ";
                }

                if ($data["kelurahan"] <> ""){
                  $alamatTmp .= $data["kelurahan"].", ";
                }

                if ($data["kecamatan"] <> ""){
                  $alamatTmp .= $data["kecamatan"].", ";
                }
    
                if ($data["kota"] <> ""){
                    $alamatTmp .= $data["kota"].", ";
                }                
    
                if ($data["provinsi"] <> ""){
                    $alamatTmp .= $data["provinsi"];
                }          
    
                $alamat_kirim .="Kepada\n";
                $alamat_kirim .="Ykh : ".$nama_kirim."\n";
                $alamat_kirim .= "Alamat : $alamatTmp \n";
                $alamat_kirim .= "No. HP : ".$no_telepon_kirim;          
            }
        }      
        
        // $alamat_pengirim ="Gudang Ethica Group"."\n\n".
        //                 "Jalan Mochammad Toha No. 398,"."\n".    
        //                 "Wates, Kecamatan Bandung Kidul,"."\n".
        //                 "Kota Bandung, Jawa Barat 40243"."\n\n".    
        //                 "No. HP : ";
        
        $sql =  "INSERT INTO keranjang_master (customer_seq, alamat_kirim, tipe_customer, alamat_pengirim, tipe_aplikasi, user_id, nama_pengirim, alamat_pengirim_lengkap, no_telepon_pengirim, ".  
                                                "nama_kirim, id_provinsi, id_kota, id_kecamatan, kelurahan, alamat_kirim_lengkap, no_telepon_kirim, ongkos_kirim, ekspedisi, service, provinsi, kota, kecamatan, ekspedisi_seq)".
                "VALUES (:customer_seq, :alamat_kirim, :tipe_customer, :alamat_pengirim, '$tipe_apps', :user_id, :nama_pengirim, :alamat_pengirim_lengkap, '', ".  
                        ":nama_kirim, :id_provinsi, :id_kota, :id_kecamatan, :kelurahan, :alamat_kirim_lengkap, :no_telepon_kirim, :ongkos_kirim, '', '', :provinsi, :kota, :kecamatan, 0)";
        $query = $db->prepare($sql);
        $nama_pengirim = "Gudang Ethica Group";
        $alamat_pengirim_lengkap = "Jalan Mochammad Toha No. 398,"."\n".    
                                                      "Wates, Kecamatan Bandung Kidul,"."\n".
                                                      "Kota Bandung, Jawa Barat 40243"."\n";

        $tipe_customer ="";
        $alamat_pengirim = $nama_pengirim ."\n".$alamat_pengirim_lengkap."\n"."No. HP : ";
        $query->bindParam(':customer_seq', $dataPost['customer_seq']);
        $query->bindParam(':tipe_customer', $tipe_customer);
        $query->bindParam(':alamat_kirim', $alamat_kirim);    
        $query->bindParam(':alamat_pengirim', $alamat_pengirim);
        $query->bindParam(':user_id', $dataPost['user_id']);        
        $query->bindParam(':nama_pengirim', $nama_pengirim);        
        $query->bindParam(':alamat_pengirim_lengkap', $alamat_pengirim_lengkap);
        $query->bindParam(':nama_kirim', $nama_kirim);
        $query->bindParam(':id_provinsi', $id_provinsi);
        $query->bindParam(':id_kota', $id_kota);
        $query->bindParam(':id_kecamatan', $id_kecamatan);
        $query->bindParam(':kelurahan', $kelurahan);
        $query->bindParam(':alamat_kirim_lengkap', $alamat_kirim_lengkap);
        $query->bindParam(':no_telepon_kirim', $no_telepon_kirim);
        $query->bindParam(':ongkos_kirim', $dataPost['ongkos_kirim']);
        //$query->bindParam(':ekspedisi', $dataPost['ekspedisi']);
        //$query->bindParam(':service', $dataPost['service']);
        $query->bindParam(':provinsi', $provinsi);
        $query->bindParam(':kota', $kota);
        $query->bindParam(':kecamatan', $kecamatan);
        $result = $query->execute();   
    }    
}

$app->get('/keranjang/load_cart_sub_agen', function (Request $request, Response $response, array $args) {    
  $customer_seq  = $request->getQueryParam("customer_seq");    
  $offset        = $request->getQueryParam("offset"); 
  $search        = $request->getQueryParam("search");

  $search = str_replace("'", "''", $search);

  $filter = "";
  if (!empty($search)){
    $filter .= " AND b.nama LIKE '%$search%'";  
  }
  $SQL_SALDO = get_sql_saldo_barang(0, " AND exists (SELECT kj.barang_seq FROM keranjang kj WHERE kj.customer_seq = $customer_seq AND b.seq = kj.barang_seq )");

  $sql =  "SELECT COUNT(*) AS jumlah_data FROM (".
            "SELECT stoks.barang_seq as barang_seq, SUM(stoks.stok) AS stok, k.qty AS jumlah FROM (".
              // "SELECT barang_seq, qty AS stok, 0 AS jumlah ".
              // "FROM penambah_pengurang_stok ".
              // "WHERE barang_seq IN (SELECT barang_seq FROM keranjang WHERE customer_seq = $customer_seq) ".
              
              // " UNION ALL ".
              
              // "SELECT barang_seq, -qty AS stok, 0 AS jumlah ".                    
              // "FROM pesanan_detail ".
              // "WHERE barang_seq IN (SELECT barang_seq FROM keranjang WHERE customer_seq = $customer_seq) ".
              // //"AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H','S') AND is_data_lama <> 'T') ".
              // "AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H')) ".
              
              // " UNION ALL ".
              
              // "SELECT barang_seq, -qty AS stok, qty AS jumlah ".                    
              // "FROM keranjang ".
              // "WHERE customer_seq = $customer_seq and qty <> 0 ".              
              
              $SQL_SALDO.

            ") AS stoks, master_barang b, keranjang k  ".
            "WHERE stoks.barang_seq = b.seq AND b.seq = k.barang_seq AND stoks.barang_seq = k.barang_seq AND k.customer_seq = $customer_seq $filter ".
            "GROUP BY stoks.barang_seq HAVING jumlah > 0 ".
          ") AS jumlah ";          
  $query = $this->db->prepare($sql);  
  $query->execute();		
  $dtJumlah     = $query->fetch();		
  $jumlah_data  = $dtJumlah["jumlah_data"];		


  $sql =  "SELECT stoks.barang_seq as barang_seq, b.nama AS nama, b.gambar_sedang AS gambar, b.harga, b.keterangan, ".
          "SUM(stoks.stok) AS stok, k.qty AS jumlah, $jumlah_data AS jumlah_data FROM (".
            // "SELECT barang_seq, qty AS stok, 0 AS jumlah ".
            // "FROM penambah_pengurang_stok ".
            // "WHERE barang_seq IN (SELECT barang_seq FROM keranjang WHERE customer_seq = $customer_seq) ".
            
            // " UNION ALL ".
            
            // "SELECT barang_seq, -qty AS stok, 0 AS jumlah ".                    
            // "FROM pesanan_detail ".
            // "WHERE barang_seq IN (SELECT barang_seq FROM keranjang WHERE customer_seq = $customer_seq) ".
            // //"AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H','S') AND is_data_lama <> 'T') ".
            // "AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H')) ".
            
            // " UNION ALL ".
            
            // "SELECT barang_seq, -qty AS stok, qty AS jumlah ".                    
            // "FROM keranjang ".
            // "WHERE barang_seq IN (SELECT barang_seq FROM keranjang WHERE customer_seq = $customer_seq) ".

            $SQL_SALDO.
            
            ") AS stoks, master_barang b, keranjang k ".
          "WHERE stoks.barang_seq = b.seq AND b.seq = k.barang_seq AND stoks.barang_seq = k.barang_seq AND k.customer_seq = $customer_seq $filter ".
          "GROUP BY stoks.barang_seq, b.nama, b.gambar, b.harga, b.keterangan, jumlah_data HAVING jumlah > 0 ".
          "ORDER BY b.nama limit 15 OFFSET $offset ";          
  $query = $this->db->prepare($sql);  
  $result = $query->execute();
  if ($result) {
    if ($query->rowCount()) {
      $data = $query->fetchAll();
    }else{
      $data = array(['barang_seq' => 0]);      
    }
  }else{
    $data = array(['barang_seq' => 0]);          
  }
    return $response->withJson($data);
});

//====================================================== API LAMA  ====================================================== \\
// $app->post('/keranjang/save_old', function (Request $request, Response $response) {
//   $dataPost = $request->getParsedBody();

//   //Save ke alamat jika belum ada di keranjang
//   $CountKeranjang = 0;
//   $querySelect = $this->db->prepare('SELECT count(*) AS jml FROM keranjang WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
//   $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);  
//   $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);  
//   $result = $querySelect->execute(); 
//   if ($result) {
//     if ($querySelect->rowCount()) {
//         $data = $querySelect->fetch();        
//         $CountKeranjang = $data["jml"];
//     }
//   }  

//   if ($CountKeranjang <= 0) {  
//     $query = $this->db->prepare('DELETE FROM keranjang_master WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
//     $query->bindParam(':customer_seq', $dataPost['customer_seq']);
//     $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);
//     $result = $query->execute(); 

//     $alamat_kirim = "";  
//     $query = $this->db->prepare("SELECT nama, alamat, kota, provinsi, no_telp FROM master_customer WHERE seq = :customer_seq");    
//     $query->bindParam(':customer_seq', $dataPost['customer_seq']);
//     $result = $query->execute();

//     if ($result) {
//       if ($query->rowCount()) {
//           $data = $query->fetch();               

//           $alamatTmp = "";
//           if ($data["alamat"] <> ""){
//             $alamatTmp .= $data["alamat"].", ";
//           }

//           if ($data["kota"] <> ""){
//             $alamatTmp .= $data["kota"].", ";
//           }

//           if ($data["provinsi"] <> ""){
//             $alamatTmp .= $data["provinsi"];
//           }          

//           $alamat_kirim .="Kepada\n";
//           $alamat_kirim .="Ykh : ".$data["nama"]."\n";
//           $alamat_kirim .= "Alamat : $alamatTmp \n";
//           $alamat_kirim .= "No. HP : ".$data["no_telp"];          
//       }
//     }
    
//     $alamat_pengirim ="Gudang Ethica Group"."\n".
//                       "BizPark Commercial Estate"."\n\n".    
//                       "Jl.Kopo Cirangrang BizPark Blok B3 No.2 dan 6"."\n".
//                       "Bandung 40227 - Jawa Barat"."\n\n".    
//                       "No. HP : ";

//     $query = $this->db->prepare("INSERT INTO keranjang_master (customer_seq, alamat_kirim, tipe_customer, alamat_pengirim, tipe_aplikasi)  
//                                 VALUES (:customer_seq, :alamat_kirim, :tipe_customer, :alamat_pengirim, 'A')");
//     $query->bindParam(':customer_seq', $dataPost['customer_seq']);
//     $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);
//     $query->bindParam(':alamat_kirim', $alamat_kirim);    
//     $query->bindParam(':alamat_pengirim', $alamat_pengirim);        
//     $result = $query->execute();   
//   }

//   //Ambil qty yang sudah tersimpan
//   $querySelect = $this->db->prepare('SELECT qty FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer');
//   $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);
//   $querySelect->bindParam(':barang_seq', $dataPost['barang_seq']);
//   $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);
//   $result = $querySelect->execute(); 
//   $rowCount = $querySelect->rowCount();   

//   if ($rowCount > 0) {  
//     $hasil = $querySelect->fetch();
//     $qty = $hasil["qty"];
//   }else{
//     $qty = 0;
//   }
  
//   //Cek stok barang
//   $qryCekStok = $this->db->prepare("SELECT stok FROM stok_barang WHERE barang_seq = :barang_seq");  
//   $qryCekStok->bindParam(':barang_seq', $dataPost['barang_seq']);  
//   $result = $qryCekStok->execute(); 
//   $rowCount = $qryCekStok->rowCount();     
  
//   if ($rowCount > 0) {  
//     $hasil = $qryCekStok->fetch();
//     if (($qty + $dataPost['qty']) > ($hasil["stok"])){
//       return $response->withJson(["status" => "stok kurang", "data" => "1", "stok" => $hasil["stok"]], 200);   
//     }else{

//       //Cek qty yang sudah tersimpan
//       $querySelect = $this->db->prepare('SELECT seq FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer');
//       $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);
//       $querySelect->bindParam(':barang_seq', $dataPost['barang_seq']);
//       $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);
//       $result = $querySelect->execute(); 
//       $rowCount = $querySelect->rowCount(); 

//       //Jika ada update jika tidak ada insert baru
//       if ($rowCount > 0) {
//         $query = $this->db->prepare('UPDATE keranjang SET qty = qty + :qty WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer');
//         $query->bindParam(':customer_seq', $dataPost['customer_seq']);
//         $query->bindParam(':barang_seq', $dataPost['barang_seq']);
//         $query->bindParam(':qty', $dataPost['qty']);
//         $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);
//         $result = $query->execute();
//         if($result)
//           return $response->withJson(["status" => "success Update", "data" => "1"], 200); 
//       }else{
//         $query = $this->db->prepare("insert into keranjang (customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi) values 
//                                     (:customer_seq, :barang_seq, :qty, :tipe_customer, 'A')");
//         $query->bindParam(':customer_seq', $dataPost['customer_seq']);
//         $query->bindParam(':barang_seq', $dataPost['barang_seq']);
//         $query->bindParam(':qty', $dataPost['qty']);
//         $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);        
//         $result = $query->execute();
//         if($result)
//           return $response->withJson(["status" => "success Save", "data" => $rowCount], 200);       
//       }  
//     }       
//   }else{    
//     return $response->withJson(["status" => "stok kurang", "data" => "1", "stok" => 0], 200);   
//   }  
// })->add($cekAPIKey);




// $app->post('/keranjang/update_qty_old', function (Request $request, Response $response) {
//   $dataPost = $request->getParsedBody();
//   if ($dataPost['tipe'] == "T"){    
//     $qryCekStok = $this->db->prepare("SELECT stok FROM stok_barang WHERE barang_seq = :barang_seq");  
//     $qryCekStok->bindParam(':barang_seq', $dataPost['barang_seq']);
//     $result = $qryCekStok->execute(); 
//     $rowCount = $qryCekStok->rowCount(); 
    
//     if ($rowCount > 0) {  
//       $hasil = $qryCekStok->fetch();
//       if (($qty + $dataPost['qty']) > ($hasil["stok"] )){
//         return $response->withJson(["status" => "stok kurang", "data" => "1"], 200);   
//       }else{
//         $query = $this->db->prepare('update keranjang set qty = :qty where seq = :seq');
//         $query->bindParam(':seq', $dataPost['seq']);
//         $query->bindParam(':qty', $dataPost['qty']);
//         $result = $query->execute();
//         if($result)
//           return $response->withJson(["status" => "success", "data" => "1"], 200);  
//       }
//     }else{
//       return $response->withJson(["status" => "stok kurang", "data" => "1"], 200);   
//     }   
//   }else{
//     $query = $this->db->prepare('update keranjang set qty = :qty where seq = :seq');
//     $query->bindParam(':seq', $dataPost['seq']);
//     $query->bindParam(':qty', $dataPost['qty']);
//     $result = $query->execute();
//     if($result)
//       return $response->withJson(["status" => "success", "data" => "1"], 200);  
//   }
// })->add($cekAPIKey);

