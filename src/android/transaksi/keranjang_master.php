<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Untuk mendapatkan keranjang master
$app->get('/keranjang_master/get/{customer_seq}/{user_id}', function (Request $request, Response $response, array $args) {
    $sql =  "SELECT customer_seq, alamat_kirim, alamat_pengirim, user_id, nama_pengirim, alamat_pengirim_lengkap, no_telepon_pengirim, nama_kirim, ".
            "id_provinsi, id_kota, id_kecamatan, provinsi, kota, kecamatan, kelurahan, alamat_kirim_lengkap, no_telepon_kirim, ongkos_kirim, ekspedisi, service, ekspedisi_seq, no_resi ".
            "FROM keranjang_master ".
            "WHERE customer_seq = :customer_seq AND user_id = :user_id ";
    $query = $this->db->prepare($sql);
    $query->bindParam(':customer_seq', $args['customer_seq']);    
    $query->bindParam(':user_id', $args['user_id']);    
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {
            $data = $query->fetchAll();
        }else{
            $data = array(
                'kode' => 200,
                'keterangan' => 'Tidak ada data',
                'data' => null);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
    return $response->withJson($data);
});


//Untuk merubah alamat tujuan di keranjang master
$app->post('/keranjang_master/update_alamat_kirim', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();

    $customer_seq         = $dataPost['customer_seq'];
    $alamat_kirim         = $dataPost['alamat_kirim'];
    //$tipe_customer        = $dataPost['tipe_customer'];    
    $user_id              = $dataPost['user_id'];
    $nama_kirim           = $dataPost['nama_kirim'];
    $id_provinsi          = $dataPost['id_provinsi'];
    $id_kota              = $dataPost['id_kota'];
    $id_kecamatan         = $dataPost['id_kecamatan'];
    $provinsi             = $dataPost['provinsi'];
    $kota                 = $dataPost['kota'];
    $kecamatan            = $dataPost['kecamatan'];
    $kelurahan            = $dataPost['kelurahan'];
    $alamat_kirim_lengkap = $dataPost['alamat_kirim_lengkap'];
    $no_telepon_kirim     = $dataPost['no_telepon_kirim'];

     
    $sql =  "UPDATE keranjang_master SET ".
                "alamat_kirim = :alamat_kirim, ".
                "nama_kirim = :nama_kirim, ".
                "id_provinsi = :id_provinsi, ".
                "id_kota = :id_kota, ".
                "id_kecamatan = :id_kecamatan, ".
                "provinsi = :provinsi, ".
                "kota = :kota, ".
                "kecamatan = :kecamatan, ".
                "kelurahan = :kelurahan, ".
                "alamat_kirim_lengkap = :alamat_kirim_lengkap, ".
                "no_telepon_kirim = :no_telepon_kirim ".
                "WHERE customer_seq = :customer_seq  AND user_id = :user_id";
    $query = $this->db->prepare($sql);    

    $query->bindParam(':customer_seq', $customer_seq);
    $query->bindParam(':alamat_kirim', $alamat_kirim);
    $query->bindParam(':user_id', $user_id);
    $query->bindParam(':nama_kirim', $nama_kirim);
    $query->bindParam(':id_provinsi', $id_provinsi);
    $query->bindParam(':id_kota', $id_kota);
    $query->bindParam(':id_kecamatan', $id_kecamatan);
    $query->bindParam(':provinsi', $provinsi);
    $query->bindParam(':kota', $kota);
    $query->bindParam(':kecamatan', $kecamatan);
    $query->bindParam(':kelurahan', $kelurahan);
    $query->bindParam(':alamat_kirim_lengkap', $alamat_kirim_lengkap);
    $query->bindParam(':no_telepon_kirim', $no_telepon_kirim);
    $result = $query->execute();   

    if($result){
        return $response->withJson(["status" => "success", "data" => "1"], 200);      
    }        
})->add($cekAPIKey);


//Untuk merubah alamat pengirim di keranjang master
$app->post('/keranjang_master/update_alamat_pengirim', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();


    $customer_seq         = $dataPost['customer_seq'];
    $alamat_pengirim      = $dataPost['alamat_pengirim'];
    $user_id              = $dataPost['user_id'];
    $nama_pengirim        = $dataPost['nama_pengirim'];
    $alamat_pengirim_lengkap = $dataPost['alamat_pengirim_lengkap'];
    $no_telepon_pengirim     = $dataPost['no_telepon_pengirim'];

    $sql = "UPDATE keranjang_master SET ".
                "alamat_pengirim = :alamat_pengirim, ".
                "nama_pengirim = :nama_pengirim, ".
                "alamat_pengirim_lengkap = :alamat_pengirim_lengkap, ".
                "no_telepon_pengirim = :no_telepon_pengirim ".                
                "WHERE customer_seq = :customer_seq AND user_id = :user_id";

    $query = $this->db->prepare($sql);
    $query->bindParam(':alamat_pengirim', $alamat_pengirim);
    $query->bindParam(':nama_pengirim', $nama_pengirim);
    $query->bindParam(':alamat_pengirim_lengkap', $alamat_pengirim_lengkap);
    $query->bindParam(':no_telepon_pengirim', $no_telepon_pengirim);
    $query->bindParam(':customer_seq', $customer_seq);
    $query->bindParam(':user_id', $user_id);    
    $result = $query->execute();   

    if($result){
        return $response->withJson(["status" => "success", "data" => "1"], 200);      
    }
})->add($cekAPIKey);


//Untuk merubah ekspedsi di keranjang master
$app->post('/keranjang_master/update_ekspedisi_ongkir', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();


    $customer_seq   = $dataPost['customer_seq'];
    $user_id        = $dataPost['user_id'];
    $ekspedisi      = $dataPost['ekspedisi'];
    $ekspedisi_seq  = $dataPost['ekspedisi_seq'];
    
    $service        = $dataPost['service'];
    $ongkos_kirim   = $dataPost['ongkos_kirim'];

    $sql = "UPDATE keranjang_master SET ".
                "ekspedisi = :ekspedisi, ".
                "service = :service, ".
                "ekspedisi_seq = :ekspedisi_seq, ".
                "ongkos_kirim = :ongkos_kirim ".                
                "WHERE customer_seq = :customer_seq AND user_id = :user_id";

    $query = $this->db->prepare($sql);
    $query->bindParam(':ekspedisi', $ekspedisi);
    $query->bindParam(':ekspedisi_seq', $ekspedisi_seq);
    $query->bindParam(':service', $service);
    $query->bindParam(':ongkos_kirim', $ongkos_kirim);
    $query->bindParam(':customer_seq', $customer_seq);
    $query->bindParam(':user_id', $user_id);
    $result = $query->execute();   

    if($result){
        return $response->withJson(["status" => "success", "data" => "1"], 200);      
    }
})->add($cekAPIKey);


//Untuk merubah ekspedsi di keranjang master
$app->post('/keranjang_master/update_no_resi', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();

    $customer_seq   = $dataPost['customer_seq'];
    $user_id        = $dataPost['user_id'];
    $no_resi        = $dataPost['no_resi'];

    $sql = "UPDATE keranjang_master SET ".
                "no_resi = :no_resi ".
                "WHERE customer_seq = :customer_seq AND user_id = :user_id";

    $query = $this->db->prepare($sql);
    $query->bindParam(':no_resi', $no_resi);    
    $query->bindParam(':customer_seq', $customer_seq);
    $query->bindParam(':user_id', $user_id);
    $result = $query->execute();   

    if($result){
        return $response->withJson(["status" => "success", "data" => "1"], 200);      
    }
})->add($cekAPIKey);


//====================================================== API LAMA  ====================================================== \\

// $app->post('/keranjang_master/update_alamat_kirim', function (Request $request, Response $response) {
//     $dataPost = $request->getParsedBody();   

//     $customer_seq  = $dataPost['customer_seq'];
//     $alamat_kirim  = $dataPost['alamat_kirim'];
//     $tipe_customer = $dataPost['tipe_customer'];    

//     $sql = "UPDATE keranjang_master SET alamat_kirim = :alamat_kirim ".
//             "WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer"
    
//     $query = $this->db->prepare();
//     $query->bindParam(':customer_seq', $customer_seq);
//     $query->bindParam(':alamat_kirim', $alamat_kirim);
//     $query->bindParam(':tipe_customer', $tipe_customer);
//     $result = $query->execute();   

//     if($result)
//         return $response->withJson(["status" => "success", "data" => "1"], 200);      
// })->add($cekAPIKey);

// $app->post('/keranjang_master/update_alamat_pengirim', function (Request $request, Response $response) {
//     $dataPost = $request->getParsedBody();

//     $customer_seq  = $dataPost['customer_seq']; 
//     $alamat_pengirim  = $dataPost['alamat_pengirim']; 
//     $tipe_customer = $dataPost['tipe_customer'];    
    
//     $query = $this->db->prepare('UPDATE keranjang_master SET alamat_pengirim = :alamat_pengirim WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
//     $query->bindParam(':customer_seq', $customer_seq);
//     $query->bindParam(':alamat_pengirim', $alamat_pengirim);
//     $query->bindParam(':tipe_customer', $tipe_customer);
//     $result = $query->execute();   

//     if($result)
//         return $response->withJson(["status" => "success", "data" => "1"], 200);      
// })->add($cekAPIKey);

// $app->get('/keranjang_master/get/{customer_seq}/{tipe_customer}', function (Request $request, Response $response, array $args) {
//     $query = $this->db->prepare('SELECT customer_seq, alamat_kirim, alamat_pengirim FROM keranjang_master where customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
//     $query->bindParam(':customer_seq', $args['customer_seq']);
//     $query->bindParam(':tipe_customer', $args['tipe_customer']);
//     $result = $query->execute();
//     if ($result) {
//         if ($query->rowCount()) {
//             $data = $query->fetchAll();
//         }else{
//             $data = array(
//                 'kode' => 200,
//                 'keterangan' => 'Tidak ada data',
//                 'data' => null);
//         }
//     }else{
//         $data = array(
//             'kode' => 100,
//             'keterangan' => 'Terdapat error',
//             'data' => null);
//     }
//     return $response->withJson($data);
//   });