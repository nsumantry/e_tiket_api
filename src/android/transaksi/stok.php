<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/stok/load_semua', function (Request $request, Response $response, array $args) {    
    $tanggal    	 = $request->getQueryParam("tanggal");
    $barang_seq    	 = $request->getQueryParam("barang_seq");
    
    $filter1 = "";
    $filter2 = "";
    $filterBarang = "";


    if ( $tanggal != ""){
        $filter1 .= " AND DATE(p.tanggal) <= DATE('$tanggal') ";
        $filter2 .= " AND DATE(m.tanggal) <= DATE('$tanggal') ";        
    }    

    if ( $barang_seq > 0){
        $filterBarang .= " AND p.barang_seq = $barang_seq ";        
    }    

    $sql = "SELECT SUM(stok) AS stok FROM (".   
                "SELECT p.qty AS stok ".
                "FROM penambah_pengurang_stok p, master_barang b  ".
                "WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter1 $filterBarang ".
                
                "UNION ALL  ".
            
                "SELECT -p.qty AS stok  ".
                "FROM pesanan_detail p, master_barang b, pesanan_master m ".
                "WHERE p.master_seq = m.seq AND ((m.status NOT IN ('T','H') AND m.jenis_so <> 'P') OR (m.status NOT IN ('K','E','B','T','H') AND m.jenis_so = 'P') ) ".
                "AND b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter2 $filterBarang ".
        ") AS barang  ";
        //die($sql);
	$query = $this->db->prepare($sql);
    $result = $query->execute();    
    $stok = 0;
    if ($result) {
        $arr = $query->fetch();    
        if ($query->rowCount()) {
            $stok = $arr["stok"];
        }
    }

    $sql = "SELECT SUM(keranjang) AS keranjang FROM (".
                "SELECT p.qty AS keranjang  ".
                "FROM keranjang p, master_barang b  ".
                "WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL AND IFNULL(b.is_preorder,'') <> 'T' $filterBarang ".    
        ") AS barang  ";	
	$query = $this->db->prepare($sql);
    $result = $query->execute();    
    $keranjang = 0;
    if ($result) {
        $arr = $query->fetch();    
        if ($query->rowCount()) {
            $keranjang = $arr["keranjang"];
        }
    }   

    //ambil stok dari tabel stok
    $sql =  "SELECT SUM(stok) AS stok FROM (".
                "SELECT (qty - terpenuhi - qty_tutup) AS stok ".
                "FROM preorder_detail p, master_barang b ".
                "WHERE p.barang_seq = b.seq AND b.tgl_hapus IS NULL $filterBarang ".

                "UNION ALL ".

                "SELECT SUM(p.stok) AS stok ".
                "FROM stok_barang p, master_barang b ".
                "WHERE p.barang_seq = b.seq AND b.tgl_hapus IS NULL $filterBarang ".
            ") AS sql1 ";            
    $query = $this->db->prepare($sql);    
    $result = $query->execute();    
    $StokTable = 0;
    if ($result) {
        $arr = $query->fetch();    
        if ($query->rowCount()) {
            $StokTable = $arr["stok"];
        }
    }
    

    $total = number_format($stok - $keranjang,0,'.','');

    $data = array(
        'jumlah' => $stok,
        'keranjang' => $keranjang,
        'total' => $total,
        'total2' => $StokTable);
    $ArResult = array();
    array_push($ArResult, $data);

  return $response->withJson($ArResult);
});



$app->get('/stok/load_perbarang', function (Request $request, Response $response, array $args) {    
    $tanggal    = $request->getQueryParam("tanggal");
    $barang_seq = $request->getQueryParam("barang_seq");
    
    $filter1 = "";
    $filter2 = "";
    $filter_barang = "";

    if ( $tanggal != ""){
        $filter1 .= " AND DATE(p.tanggal) <= DATE('$tanggal') ";
        $filter2 .= " AND DATE(m.tanggal) <= DATE('$tanggal') ";        
    }  

    if ($barang_seq > 0){
        $filter_barang .= " AND p.barang_seq = $barang_seq ";        
    }

    $sql = "SELECT barang_seq, SUM(stok) AS stok FROM ( ".
                "SELECT barang_seq, SUM(stok) AS stok FROM (".   
                    "SELECT p.barang_seq, p.qty AS stok ".
                    "FROM penambah_pengurang_stok p, master_barang b  ".
                    "WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter1 $filter_barang ".
                    
                    "UNION ALL  ".
                
                    "SELECT p.barang_seq, -p.qty AS stok  ".
                    "FROM pesanan_detail p, master_barang b, pesanan_master m ".
                    "WHERE p.master_seq = m.seq AND ((m.status NOT IN ('T','H') AND m.jenis_so <> 'P') OR (m.status NOT IN ('K','E','B','T','H') AND m.jenis_so = 'P') ) ".
                    "AND b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter2 $filter_barang ".
                    
                    "UNION ALL  ".

                    "SELECT p.barang_seq, -p.qty AS stok ".	 
                    "FROM keranjang p, master_barang b ".
                    "WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL AND IFNULL(b.is_preorder,'') <> 'T' $filter_barang ".
                ") AS barang GROUP BY barang_seq ".
            ") AS barang1 GROUP BY barang_seq ".
            "ORDER BY barang_seq ";
    $query = $this->db->prepare($sql);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {
            $data = $query->fetchAll();
        }else{
            $data = array();
        }
    }else{
            $data = array();
    }
    return $response->withJson($data);
});


$app->get('/stok/load_keranjang', function (Request $request, Response $response, array $args) {        
    $barang_seq = $request->getQueryParam("barang_seq");    
    
    $filter_barang = "";    

    if ($barang_seq > 0){
        $filter_barang .= " AND barang_seq IN ( $barang_seq )";        
    }

    $sql =  "SELECT seq, customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi, user_id, is_ceklis, qty_order, qty_terpesan, tgl_input, diskon ".
            "FROM keranjang WHERE seq <> 0 $filter_barang ORDER BY barang_seq";
    $query = $this->db->prepare($sql);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {
            $data = $query->fetchAll();
        }else{
            $data = array();
        }
    }else{
            $data = array();
    }
    return $response->withJson($data);
});

$app->get('/stok/cek', function (Request $request, Response $response, array $args) {        
    $dtbase = $this->db;
    $sql = getStokBarang($dtbase, 12132, "");
    die($sql);
});