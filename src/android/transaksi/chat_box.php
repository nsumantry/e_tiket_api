<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Untuk menyimpan chat
$app->post('/chat_box/save', function (Request $request, Response $response) {
    $dataPost    = $request->getParsedBody();

    $pesanan_seq = $dataPost['pesanan_seq'];
    
    $sql =  "SELECT is_selesai_chat FROM pesanan_master WHERE seq = $pesanan_seq ";    
    $qry = $this->db->prepare($sql);    
    $qry->execute(); 
    $hasil = $qry->fetch();
    $is_selesai_chat = $hasil["is_selesai_chat"];

    if ($is_selesai_chat == "T"){
        return $response->withJson(["status" => "chat selesai"], 200);
    }               
    
    $sql =	"INSERT INTO chat_box (tanggal, pesanan_seq, isi_pesan, tipe_pesan, user_id, is_baca, is_execute) ".
            "VALUES(now(), :pesanan_seq, :isi_pesan, :tipe_pesan, :user_id, 'F', 'F')";    
    $query = $this->db->prepare($sql);        
    $query->bindParam(':pesanan_seq', $dataPost['pesanan_seq']);
    $query->bindParam(':isi_pesan', $dataPost['isi_pesan']);
    $query->bindParam(':tipe_pesan', $dataPost['tipe_pesan']);
    $query->bindParam(':user_id', $dataPost['user_id']);    
    $result = $query->execute();
    if ($result){
        return $response->withJson(["status" => "success"], 200);
    }else{
        return $response->withJson(["status" => "gagal"], 200);   
    }  	
})->add($cekAPIKey);

//Untuk update chat status chat sudah terbaca
$app->post('/chat_box/update_baca', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();
    $pesanan_seq  = $request->getQueryParam("pesanan_seq");    

    $sql =	"UPDATE chat_box set is_baca = 'T' WHERE pesanan_seq = :pesanan_seq AND tipe_pesan = 'D'";            
    $query = $this->db->prepare($sql);    
    $query->bindParam(':pesanan_seq', $dataPost['pesanan_seq']);    
    $result = $query->execute();
    if ($result){
        return $response->withJson(["status" => "success"], 200);   
    }else{
        return $response->withJson(["status" => "gagal"], 200);   
    }  	
})->add($cekAPIKey);


//Untuk mendapatkan daftar chating berdasarkan pesanan
$app->get('/chat_box/load', function (Request $request, Response $response, array $args) {
    $pesanan_seq  = $request->getQueryParam("pesanan_seq");    
    $offset       = $request->getQueryParam("offset"); 	

    $Filter = "";
    if ($offset > 0){
        $Filter .= " LIMIT 30 OFFSET $offset ";
    }

    if (empty($pesanan_seq)){
        return $response->withJson(["status" => "gagal", "seq" => 0], 200);   		
    }    

    $sql = 	"SELECT COUNT(*) AS jumlah_data ".
			"FROM chat_box ".
            "WHERE pesanan_seq = $pesanan_seq AND isi_pesan <> '' ";            
    $query = $this->db->prepare($sql);
    $query->execute();		
    $dtJumlah     = $query->fetch();		
    $jumlah_data  = $dtJumlah["jumlah_data"];

	$sql = 	"SELECT seq, tanggal, pesanan_seq, isi_pesan, tipe_pesan, user_id, is_baca, is_execute, $jumlah_data AS jumlah_data ".
			"FROM chat_box ".
            "WHERE pesanan_seq = $pesanan_seq AND isi_pesan <> '' ORDER BY tanggal $Filter";            
  	$query = $this->db->prepare($sql);
	$result = $query->execute();
    
    if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});


$app->get('/chat_box/load_sink_desktop', function (Request $request, Response $response, array $args) {	 	
    $seq = $request->getQueryParam("seq"); 	
	$sql = "SELECT seq, tanggal, pesanan_seq, isi_pesan, tipe_pesan, user_id, is_baca, is_execute ".
           "FROM chat_box m WHERE seq > $seq AND tipe_pesan <> 'D' ";           
  	$query = $this->db->prepare($sql);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array();
		}
	}else{
			$data = array();
	}
  	return $response->withJson($data);
});


//Untuk mendapatkan jumlah chat yang belum terbaca
$app->get('/chat_box/getcount', function (Request $request, Response $response, array $args) {    
    $filter = "";    
    $customer_seq = $request->getQueryParam("customer_seq");        
    $user_id      = $request->getQueryParam("user_id");        

    $sql =  "SELECT COUNT(*) as total ".
            "FROM chat_box ".
            "WHERE is_baca = 'F' AND pesanan_seq IN ( SELECT seq FROM pesanan_master WHERE customer_seq = $customer_seq AND user_id = '$user_id' AND isi_pesan <> '' AND is_selesai_chat = 'F' AND status NOT IN('T','H'))".
            "AND tipe_pesan = 'D' ";
    $query = $this->db->prepare($sql);        
    $result = $query->execute();
    if ($result) {
      if ($query->rowCount()) {
        $data = $query->fetchAll();
      }else{
        $data = array(
          'kode' => 200,
          'keterangan' => 'Tidak ada data',
          'data' => null);
      }
    }else{
      $data = array(
        'kode' => 100,
        'keterangan' => 'Terdapat error',
        'data' => null);
    }
    return $response->withJson($data);
});