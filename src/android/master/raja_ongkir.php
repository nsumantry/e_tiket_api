<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/raja_ongkir/kurir', function (Request $request, Response $response, array $args) {		
    $courier = $request->getQueryParam("courier");	    
	
	$url = "https://pro.rajaongkir.com/api/cost";
	$fields = array(
		'key' => '3afce7052c726488d68120396c032b34',
		'origin' => 151,
		'originType' => 'city',		
		'destination' => 23,
		'destinationType' => 'city',		
		'weight' => 1,
		'courier' => $courier
	);

	$fields_string ="";
	foreach($fields as $key=>$value) { 
		$fields_string .= $key.'='.$value.'&'; 
	}
	rtrim($fields_string, '&');
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);            
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_POST, count($fields));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

	$raw_data = curl_exec($ch);	 
	curl_close($ch);
	
	$data = json_decode($raw_data, true);	
	//die(var_dump($data));

	$ArResult = array();		
	foreach ( $data['rajaongkir']['results'] as $key1 =>$value1)
	{   
		$code = $value1['code']; 
		$name = $value1['name'];

		foreach ( $value1['costs'] as $key2 =>$value2)
		{   
			$service 	 = $value2['service'];
			$description = $value2['description'];

			foreach ( $value2['cost'] as $key3 =>$value3)
			{   
				$value 	= $value3['value'];
				$etd	= $value3['etd'];
				$note	= $value3['note'];

				$isi = array(
					'code' 			=> $code,
					'name' 			=> $name,
					'service' 		=> $service,
					'description' 	=> $description,
					'value' 		=> $value,
					'etd' 			=> $etd,
					'note' 			=> $note
				);
				array_push($ArResult, $isi);				
			}
		}
	}

	return $response->withJson($ArResult);
});


$app->get('/raja_ongkir/province', function (Request $request, Response $response, array $args) {		
	$url = "https://pro.rajaongkir.com/api/province?key=3afce7052c726488d68120396c032b34";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);            
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 

	$raw_data = curl_exec($ch);	 
	curl_close($ch);
	
	$data = json_decode($raw_data, true);	
	
	$ArResult = array();

	if (!empty($data)){		
		foreach ( $data['rajaongkir']['results'] as $key1 =>$value1)
		{ 
			$province_id = $value1['province_id'];
			$province 	 = $value1['province']; 

			$isi = array(
				'province_id' => $province_id,
				'province' 	  => $province
			);
			array_push($ArResult, $isi);				
		}	
	}

	return $response->withJson($ArResult);
});

$app->get('/raja_ongkir/city', function (Request $request, Response $response, array $args) {		
	$id			  = $request->getQueryParam("id");	
	$province = $request->getQueryParam("province");	

	$filter = "";
	
	if (!empty($id)){
		$filter .= "&id=$id";
	}

	if (!empty($province)){
		$filter .= "&province=$province";
	}
	
	$url = "https://pro.rajaongkir.com/api/city?key=3afce7052c726488d68120396c032b34".$filter;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);            
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 

	$raw_data = curl_exec($ch);	 
	curl_close($ch);
	
	$data = json_decode($raw_data, true);
	$ArResult = array();

	if (!empty($data)){
		$ArResult = $data['rajaongkir']['results']; 
	}

	return $response->withJson($ArResult);
});


$app->get('/raja_ongkir/subdistrict', function (Request $request, Response $response, array $args) {		
	$id	  = $request->getQueryParam("id");	
	$city = $request->getQueryParam("city");	

	$filter = "";
	
	if (!empty($id)){
		$filter .= "&id=$id";
	}

	if (!empty($city)){
		$filter .= "&city=$city";
	}
	
	$url = "https://pro.rajaongkir.com/api/subdistrict?key=3afce7052c726488d68120396c032b34".$filter;	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);            
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 

	$raw_data = curl_exec($ch);	 
	curl_close($ch);
	
	$data = json_decode($raw_data, true);

	$ArResult = array();

	if (!empty($data)){
		$ArResult = $data['rajaongkir']['results']; 
	}

	return $response->withJson($ArResult);
});