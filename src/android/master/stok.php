<?php
use Slim\Http\Request;
use Slim\Http\Response;


$app->get('/stok/get/{barcode}', function (Request $request, Response $response, array $args) {
  $query = $this->db->prepare('SELECT * FROM master_barang where barcode = :barcode');
  $query->bindParam(':barcode', $args['barcode']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});