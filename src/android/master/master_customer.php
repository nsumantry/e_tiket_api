<?php
use Slim\Http\Request;
use Slim\Http\Response;


// $app->post('/master_customer/login', function (Request $request, Response $response, array $args) {
//     $data = $request->getParsedBody();

//     $user_id = $data['user'];
//     $pass    = $data['pass'];
//     $tipe    = "";

//     $querySelect = $this->db->prepare("SELECT seq FROM master_customer
//                                        WHERE user_id_ethica = '$user_id' AND password_ethica = '$pass' and is_ethica = 'T' 
//                                        and tgl_hapus is null AND user_id_ethica IS NOT NULL AND user_id_ethica <> '' ");

//     $result = $querySelect->execute(); 
//     $rowCount = $querySelect->rowCount(); 
//     $tipe = "E";
//     if ($rowCount ==  0) {
//         $querySelect = $this->db->prepare("SELECT seq FROM master_customer 
//                                            WHERE user_id_seply = '$user_id' AND password_seply = '$pass' and is_seply = 'T' 
//                                            and tgl_hapus is null AND user_id_ethica IS NOT NULL AND user_id_seply <> '' ");
//         $result = $querySelect->execute();  
//         $rowCount = $querySelect->rowCount(); 
//         $tipe = "S";
//     }

//     if ($rowCount > 0) {
//         $hasil = $querySelect->fetch();
//        $seq = $hasil["seq"];
//     }
//     if ((($user_id === 'orion') && ($pass === 'bandung'))){
//         $seq = 777777;   
//     }

//     if ($seq > 0) {

//         $query = $this->db->prepare("delete from api_users where user_id = '$user_id' and password = '$pass' and tipe_aplikasi = 'A'");
//         $result = $query->execute();

//         $api_key = RandomToken(32);
//         $api_key = substr($api_key, 0, 31);

//         $query = $this->db->prepare("insert into api_users (user_id, password, api_key, expired_date, tipe_aplikasi) values 
//                                     (:user_id, :pass, :api_key, (SELECT Date(now())+ INTERVAL 1 MINUTE + INTERVAL 1 DAY), 'A')");
//         $query->bindParam(':user_id', $data['user']);
//         $query->bindParam(':pass', $data['pass']);
//         $query->bindParam(':api_key', $api_key);
//         $result = $query->execute();
//         if($result){        
//             return $response->withJson([
//                 "id" => $seq, 
//                 "api_key" => $api_key,
//                 "status"=>"success",
//                 "tipe"=>"$tipe"
//                 ], 200);       
//         }else{
//             return $response->withJson([
//                 "id" => "0", 
//                 "api_key" => "",
//                 "status"=>"Failed",
//                 "tipe"=>"$tipe"
//                 ], 200);       
//         }

//     }else{
//         return $response->withJson([
//             "id" => "0", 
//             "api_key"=> "",
//             "status"=>"Failed",
//             "tipe"=>""
//             ], 200);       
//     }
// });


//Digunakan untuk login juga mendapatkan jenis keagenan dan api_key
$app->post('/master_customer/login', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();

    $user_id = $data['user'];
    $pass    = $data['pass'];        
    $tipe_apps    = $data['tipe_apps'];

    if(!isset($tipe_apps)){
        return $response->withJson(["status" => "Tipe Apps required"]);
    }

    if(($tipe_apps != "W") && ($tipe_apps != "A")){
        return $response->withJson(["status" => "Tipe Apps vailed"]);
    }

    $sql = "SELECT d.seq, d.master_seq, d.is_ethica, d.is_seply, d.is_ethica_hijab, m.jenis ".
           "FROM detail_customer d, master_customer m ".
           "WHERE d.user_id = '$user_id' AND d.passwd = '$pass' AND d.tgl_hapus is null AND m.tgl_hapus is null AND d.master_seq = m.seq ";           
    $querySelect = $this->db->prepare($sql);
    $result = $querySelect->execute(); 
    $rowCount = $querySelect->rowCount();     
    $seq = 0;

    if ($rowCount > 0) {
       $hasil           = $querySelect->fetch();
       $seq             = $hasil["seq"];
       $master_seq      = $hasil["master_seq"];
       $is_ethica       = $hasil["is_ethica"];
       $is_seply        = $hasil["is_seply"];
       $is_ethica_hijab = $hasil["is_ethica_hijab"];
       $jenis           = $hasil["jenis"];
    }

    if ((($user_id === 'orion') && ($pass === 'bandung'))){
        $seq = 777777;
    }

    if ($seq > 0) {
        $sql = "DELETE FROM api_users WHERE user_id = '$user_id' AND password = '$pass' AND tipe_aplikasi = '$tipe_apps'";
        $query = $this->db->prepare($sql);
        $result = $query->execute();

        $api_key = RandomToken(32);
        $api_key = substr($api_key, 0, 31);

        $query = $this->db->prepare("insert into api_users (user_id, password, api_key, expired_date, tipe_aplikasi) values 
                                    (:user_id, :pass, :api_key, (SELECT Date(now())+ INTERVAL 1 MINUTE + INTERVAL 1 DAY), '$tipe_apps')");
        $query->bindParam(':user_id', $data['user']);
        $query->bindParam(':pass', $data['pass']);
        $query->bindParam(':api_key', $api_key);
        $result = $query->execute();

        if($result){        
            return $response->withJson([
                "id" => $seq, 
                "id_customer"     => $master_seq, 
                "is_ethica"       => $is_ethica,
                "is_seply"        => $is_seply,
                "is_ethica_hijab" => $is_ethica_hijab,                
                "api_key"         => $api_key,
                "jenis"           => $jenis,
                "status"=>"success"                
                ], 200);       
        }else{
            return $response->withJson([
                "id"              => "0", 
                "id_customer"     => "0",                 
                "is_ethica"       => "",
                "is_seply"        => "",
                "is_ethica_hijab" => "",
                "api_key"         => "",
                "jenis"           => "",
                "status"          =>"Failed"
                ], 200);       
        }
    }else{
        return $response->withJson([
            "id"              => "0", 
            "id_customer"     => "0",                 
            "is_ethica"       => "",
            "is_seply"        => "",
            "is_ethica_hijab" => "",
            "api_key"         => "",
            "jenis"           => "",
            "status"          =>"Failed"                
            ], 200);         
    }
});


//Untuk mendapatkan profile customer
$app->get('/master_customer/get/{id}', function (Request $request, Response $response, array $args) {
  $query = $this->db->prepare('SELECT seq, kode, nama, no_telp, NO_TELP_2, alamat, kota, provinsi, npwp, top, plafon, tgl_hapus, user_id_ethica, user_id_seply, email, kecamatan FROM master_customer where seq = :id');
  $query->bindParam(':id', $args['id']);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {
            $data = $query->fetchAll();
        }else{
            $data = array(
                'kode' => 200,
                'keterangan' => 'Tidak ada data',
                'data' => null);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
    return $response->withJson($data);
});


//Untuk merubah password
$app->post('/master_customer/change_password', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();    

    $customer_seq = $data['customer_seq'];
    $pass_lama    = $data['pass_lama'];
    $pass_baru    = $data['pass_baru'];
    $user_id      = $data['user_id'];
    $tipe_apps    = $data['tipe_apps'];


    $sql =  "SELECT user_id FROM detail_customer ".
            "WHERE master_seq = $customer_seq AND passwd = '$pass_lama' AND user_id = '$user_id' ";
    $querySelect = $this->db->prepare($sql);
    $querySelect->execute();
     
    $rowCount = $querySelect->rowCount(); 
    if ($rowCount ==  0) {
        return $response->withJson(["status"=>"Wrong Password"], 200);   
    }
    
    $hasil = $querySelect->fetch();
    $user_id = $hasil["user_id"];

    $sql = "UPDATE detail_customer SET passwd = '$pass_baru' WHERE master_seq = $customer_seq AND user_id = '$user_id' ";
    $query = $this->db->prepare($sql);
    $query->execute();

    $sql = "UPDATE api_users set password = '$pass_baru' WHERE user_id = '$user_id' and tipe_aplikasi = '$tipe_apps'";
    $query = $this->db->prepare($sql);
    $result = $query->execute();

    if($result){        
        return $response->withJson([
            "status"=>"success"
            ], 200);       
    }else{
        return $response->withJson([
            "status"=>"Failed"
            ], 200);       
    }
})->add($cekAPIKey);


//Untuk mendapatkan daftar customer yang terdaftar sebagai sub agen dari customer agen
$app->get('/master_customer/load_sub_agen', function (Request $request, Response $response, array $args) {
	$search       = $request->getQueryParam("search");
	$customer_seq = $request->getQueryParam("customer_seq");		
	$offset       = $request->getQueryParam("offset");	

	$search = str_replace("'", "''", $search);

	$filter = "";
	if (!empty($search)){
		$filter .= " AND ((nama LIKE '%$search%') OR (no_telp LIKE '%$search%') OR (alamat LIKE '%$search%') OR (kecamatan LIKE '%$search%') OR (kota LIKE '%$search%') OR (provinsi LIKE '%$search%'))";
    }

    $sql = 	"SELECT COUNT(*) AS jumlah_data ".
			"FROM master_customer ".
			"WHERE cust_induk_seq = $customer_seq $filter ";
    $query = $this->db->prepare($sql);
    $query->execute();		
    $dtJumlah     = $query->fetch();		
    $jumlah_data  = $dtJumlah["jumlah_data"];

	$sql = 	"SELECT seq, kode, nama, no_telp, alamat, kecamatan, kota, provinsi, jenis, $jumlah_data AS jumlah_data ".
			"FROM master_customer ".
			"WHERE cust_induk_seq = $customer_seq $filter ORDER BY seq DESC limit 15  OFFSET $offset ";		
    $query = $this->db->prepare($sql);    
	  
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});


//Untuk mendapatkan data customer yang terdaftar sebagai sub agen dari customer agen
$app->get('/master_customer/get_sub_agen', function (Request $request, Response $response, array $args) {	
    $customer_seq = $request->getQueryParam("customer_seq");		
    
    $sqlJumlah = "SELECT IFNULL(SUM(qty),0) FROM keranjang WHERE customer_seq = $customer_seq";
    	
	$sql = 	"SELECT seq, kode, nama, no_telp, alamat, kecamatan, kota, provinsi, jenis, ($sqlJumlah) AS jumlah ".
			"FROM master_customer ".
			"WHERE seq = $customer_seq ";
	$query = $this->db->prepare($sql);
	  
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});


//Untuk mendapatkan keagenan customer
$app->get('/master_customer/get_keagenan', function (Request $request, Response $response, array $args) {	
    $customer_seq = $request->getQueryParam("customer_seq");
    $user_id      = $request->getQueryParam("user_id");
    
    $sql = "SELECT seq, is_ethica, is_seply, is_ethica_hijab FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id' ";   		    
	$query = $this->db->prepare($sql);	  
    
    $result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});


//Untuk mendapatkan keagenan customer
$app->get('/master_customer/is_keagenan_ethica_hijab', function (Request $request, Response $response, array $args) {   
    $customer_seq = $request->getQueryParam("customer_seq");
    
    $sql = "SELECT * FROM customer_keagenan_detail WHERE master_seq = $customer_seq and keagenan_seq = 3 ";              
    $query = $this->db->prepare($sql);    
    
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount() > 0) {
            $data = array(['hasil' => 'T']);
        }else{
            $data = array(['hasil' => 'F']);
        }
    }else{
            $data = array(['hasil' => 'F']);
    }
  return $response->withJson($data);
});