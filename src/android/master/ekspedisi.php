<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Digunakan untuk mendapatkan daftar ekspedisi yang digunakan oleh ethica
$app->get('/ekspedisi/load', function (Request $request, Response $response, array $args) {

    $sql =  "SELECT m.seq, m.kode AS kode, m.nama, d.service AS service, d.deskripsi, 1 AS urutan ".
            "FROM master_ekspedisi m, detail_ekspedisi d ".
			"WHERE m.seq = d.master_seq AND m.tgl_hapus IS NULL AND m.kode <> 'LAIN - LAIN' ".
			" UNION ALL ".
			"SELECT m.seq, m.kode AS kode, m.nama, d.service AS service, d.deskripsi, 2 AS urutan ".
            "FROM master_ekspedisi m, detail_ekspedisi d ".
			"WHERE m.seq = d.master_seq AND m.tgl_hapus IS NULL AND m.kode = 'LAIN - LAIN' ".
			"ORDER BY urutan, kode, service";	
	$query = $this->db->prepare($sql);					
	$result = $query->execute();
	
	if ($result) {
        if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
        }
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});