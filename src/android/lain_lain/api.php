<?php
use Slim\Http\Request;
use Slim\Http\Response;

// $cekAPIKey = function($request, $response, $next){
//     $key = $request->getQueryParam("key");    
//     if(!isset($key)){
//         return $response->withJson(["status" => "API Key required"]);
//     }
    
//     $sql = "SELECT * FROM api_users WHERE api_key=:api_key and expired_date > now() and tipe_aplikasi = 'A'";
//     $stmt = $this->db->prepare($sql);
//     $stmt->execute([":api_key" => $key]);
    
//     if($stmt->rowCount() > 0){
//         $result = $stmt->fetch();
//         if($key == $result["api_key"]){        
//             $stmt->execute([":api_key" => $key]);
//             $user_id = $result["user_id"];
//             if(strtoupper($user_id) != "ORION"){                
//                 //Cek apakah usernya masih aktif atau tidak
//                 $sql =  "SELECT d.seq FROM detail_customer d, master_customer c ".
//                         "WHERE d.user_id = '$user_id' AND d.tgl_hapus IS NULL AND c.seq = d.master_seq AND c.tgl_hapus IS NULL ";
//                 $qry = $this->db->prepare($sql);
//                 $qry->execute();
                
//                 if($qry->rowCount() > 0){
//                     return $response = $next($request, $response);
//                 }else{
//                     return $response->withJson(["status" => "Unauthorized"]);                
//                 }
//             }
//             return $response = $next($request, $response);            
//         }
//     }
//     return $response->withJson(["status" => "Unauthorized"]);
// };    


$cekAPIKey = function($request, $response, $next){
    $dataPost = $request->getParsedBody();

    $key       = $request->getQueryParam("key");
    $versi     = $request->getQueryParam("versi");
    $tipe_apps = $dataPost['tipe_apps'];
    
    if ($tipe_apps ==""){
        $tipe_apps = $request->getQueryParam("tipe_apps");
    }
    
    
    if(!isset($key)){
        return $response->withJson(["status" => "API Key required"]);
    }    
    
    $sql = "SELECT * FROM api_users WHERE api_key=:api_key and expired_date > now()";
    $stmt = $this->db->prepare($sql);
    $stmt->execute([":api_key" => $key]);
    
    if($stmt->rowCount() > 0){
        $result = $stmt->fetch();
        if($key == $result["api_key"]){            
            $user_id = $result["user_id"];

            if(strtoupper($user_id) != "ORION"){                

                if(!isset($tipe_apps)){
                    return $response->withJson(["status" => "Tipe Apps required"]);
                }
            
                if(($tipe_apps != "W") && ($tipe_apps != "A")){
                    return $response->withJson(["status" => "Tipe Apps vailed"]);
                }

                if ($tipe_apps =="A"){
                    if(!isset($versi)){
                        return $response->withJson(["status" => "Versi required"]);
                    }                                    
                    
                    //Cek Versi                
                    $sql =  "SELECT tanggal, versi, versi_name FROM versi_program ";
                    $qry = $this->db->prepare($sql);
                    $qry->execute();
                    $arr = $qry->fetch();
    
                    $versiApp = $arr["versi"];
                    if ($versiApp > $versi){
                        return $response->withJson(["status" => "harus update"]);
                    }
                }

                //Cek apakah usernya masih aktif atau tidak
                $sql =  "SELECT d.seq FROM detail_customer d, master_customer c ".
                        "WHERE d.user_id = '$user_id' AND d.tgl_hapus IS NULL AND c.seq = d.master_seq AND c.tgl_hapus IS NULL ";
                $qry = $this->db->prepare($sql);
                $qry->execute();
                
                if($qry->rowCount() > 0){
                    return $response = $next($request, $response);
                }else{
                    return $response->withJson(["status" => "Unauthorized"]);
                }
            }
            return $response = $next($request, $response);            
        }
    }
    return $response->withJson(["status" => "Unauthorized"]);
};    


$cekAPIKeyArray = function($request, $response, $next){

    $key = $request->getQueryParam("key");    

    if(!isset($key)){
        return $response->withJson(array(["status" => "API Key required"]));
    }
    
    $sql = "SELECT * FROM api_users WHERE api_key=:api_key and expired_date > now() and tipe_aplikasi = 'A'";
    $stmt = $this->db->prepare($sql);
    $stmt->execute([":api_key" => $key]);
    
    if($stmt->rowCount() > 0){
        $result = $stmt->fetch();
        if($key == $result["api_key"]){
        
            $stmt->execute([":api_key" => $key]);
            
            return $response = $next($request, $response);
        }
    }

    return $response->withJson(array(["status" => "Unauthorized"]));
};

$app->get('/api/cek', function (Request $request, Response $response) {
    return $response->withJson(["status" => "Authorized"]);        
})->add($cekAPIKey);

function RandomToken($length = 32){
    if(!isset($length) || intval($length) <= 8 ){
      $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    } 
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

