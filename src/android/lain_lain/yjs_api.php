<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/yjs_api/get_sarimbit/{seq}', function (Request $request, Response $response, array $argss){
    $sql = "SELECT m.seq, m.nama, m.tgl_berlaku_dari, m.tgl_berlaku_sampai, m.kelompok_brand, m.keterangan, m.user_id, m.tgl_hapus, m.gambar, m.gambar_sedang, m.gambar_besar ".
           "FROM setting_sarimbit_master m ".
           "WHERE seq = :seq ";
    $query = $this->db->prepare($sql);
	$query->bindParam(':seq', $argss['seq']);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {        
            if ($query->rowCount()) {
                $data = $query->fetchAll();
            }else{
                $data = array(['seq' => 0]);
            }       
        }else{
            $data = array([
            'seq' => 0,
            'kode' => 200,
            'keterangan' => 'Tidak ada data',
            'data' => null]);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
    return $response->withJson($data);
  });


  $app->get('/yjs_api/get_barang/{seq}', function (Request $request, Response $response, array $args) {
    $db   = $this->db;
    $stok = getStokBarang($db, $args['seq'], 0);
	$sql = "SELECT seq, barcode, nama, brand, sub_brand, warna, ukuran, katalog, tahun, tipe_brg, gambar, harga, tgl_hapus, ".
			"gambar_besar, gambar_sedang, keterangan, tgl_release, is_ehijab, is_preorder, berlaku_dari, berlaku_sampai, jumlah_pesan, ".
			"brand_seq, sub_brand_seq, berat, klasifikasi, $stok AS stok ".
            "FROM master_barang WHERE seq = :seq ";        

	$query = $this->db->prepare($sql);
	$query->bindParam(':seq', $args['seq']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});