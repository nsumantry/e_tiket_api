<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->post('/dml/insert', function (Request $request, Response $response) {

    $dml = $request->getParsedBody();

    $sql  = $dml['sql_1'];
    $sql2 = $dml['sql_2'];
	$sql3 = $dml['sql_3'];

	// $sql = str_replace("EQUALORION","=",$sql);
	// $sql2 = str_replace("EQUALORION","=",$sql2);
	// $sql3 = str_replace("EQUALORION","=",$sql3);

    if ($sql2 != ''){
			$stmt2 = $this->db->prepare($sql2); 
			$cek = $stmt2->execute();

			if ($cek) {
				if ($stmt2->rowCount() > 0) {
    			$query2 = $this->db->prepare($sql3); 
		    	if($query2->execute())
		       	return $response->withJson(["status" => "success", "data" => "1"], 200);   
				}else{
					$query2 = $this->db->prepare($sql); 
		    	if($query2->execute())
		       	return $response->withJson(["status" => "success", "data" => "1"], 200);   
				}
			}else{
				$query2 = $this->db->prepare($sql); 
		    	if($query2->execute())
		       	return $response->withJson(["status" => "success", "data" => "1"], 200);   
			} 	
    }else{
    	$stmt = $this->db->prepare($sql);
    	if($stmt->execute())
       	return $response->withJson(["status" => "success", "data" => "1"], 200);    	
    }
})->add($cekAPIKey);



$app->post('/lain/upload', function(Request $request, Response $response, $args) {
	try {    
		$uploadedFiles = $request->getUploadedFiles();		
		
		// handle single input with single file upload
		$uploadedFile = $uploadedFiles['gambar'];
		if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
			$extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
			
			$filename = sprintf('%s.%0.8s', "2", $extension);
			
			$dir = $this->get('settings')['upload_directory'];

			$directory = $dir."/".$request->getQueryParam("lokasi");
			$nama_file = $request->getQueryParam("nama_file");
			if (!file_exists($directory)) {
				mkdir($directory, 0777, true);
			}
			$uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $nama_file);
			if (file_exists($directory . DIRECTORY_SEPARATOR . $nama_file)) {
				return $response->withJson(["status" => "success", "data" => "1"], 200);
			}else{
				return $response->withJson(["status" => "gagal", "data" => "1"], 200);    				
			}
		}
	}catch(Exception $e) {			
		return $response->withJson(["status" => "gagal", "data" => "1"], 200);    		
	}	
});


//new
$app->post('/dml/insert_new', function (Request $request, Response $response) {

	$dml = $request->getParsedBody();
	
	$seq  			 = $dml['seq'];
	$sql_1			 = $dml['sql_1'];
    $sql_2			 = $dml['sql_2'];
	$sql_3			 = $dml['sql_3'];
    $group_kirim_seq = $dml['group_kirim_seq'];
	$jumlah_group	 = $dml['jumlah_group'];
	$urutan_group 	 = $dml['urutan_group'];    

	$db   = $this->db;
	try {				
		$db->beginTransaction();
		// $sql  = "SELECT * FROM histori_dml WHERE last_dml_seq > $seq";
		// $stmt = $db->prepare($sql);		
		// $stmt->execute();
		
		//if($stmt->rowCount() == 0){
			$sql = "INSERT INTO dml_temp(seq, sql_1, sql_2, sql_3, group_kirim_seq, urutan_group, jumlah_group) ".
								"VALUES (:seq, :sql_1, :sql_2, :sql_3, :group_kirim_seq, :urutan_group, :jumlah_group)";
			$query = $db->prepare($sql);
			$query->bindParam(':seq', $seq);
			$query->bindParam(':sql_1', $sql_1);
			$query->bindParam(':sql_2', $sql_2);
			$query->bindParam(':sql_3', $sql_3);
			$query->bindParam(':group_kirim_seq', $group_kirim_seq);
			$query->bindParam(':urutan_group', $urutan_group);
			$query->bindParam(':jumlah_group', $jumlah_group);
			$query->execute();		

			// $query = $db->prepare("UPDATE histori_dml SET last_dml_seq = $seq");
			// $query->execute();
		//}
		$db->commit();
	} catch(PDOException $pdoe) {
		$db->rollBack();
		if (strpos($pdoe, 'Integrity constraint violation') !== false) {
			return $response->withJson(["status" => "success constraint"], 200);  
		}else{
			return $response->withJson(["status" => "gagal"], 100);  
		}
	}catch(Exception $e) {	
		$db->rollBack();
		return $response->withJson(["status" => "gagal"], 100);  
	}
	return $response->withJson(["status" => "success"], 200);  	    
})->add($cekAPIKey);

//new
$app->post('/dml/execute', function (Request $request, Response $response) {
	//cari group kirim yang sudah komplit
	$db   = $this->db;
	//$sql = "SELECT DISTINCT group_kirim_seq FROM dml_temp WHERE jumlah_group = urutan_group  ORDER BY seq";
	$sql = "SELECT group_kirim_seq, jumlah_group, COUNT(*) FROM dml_temp GROUP BY group_kirim_seq, jumlah_group HAVING COUNT(*) = jumlah_group ORDER BY group_kirim_seq";	
    $stmt = $db->prepare($sql);
	$stmt->execute();
	
	$JalankanUpdate = false;

	$array = $stmt->fetchAll();	
	$seq = 0;
    try {
		$db->beginTransaction();
		foreach($array as $row) {
        	$seq = $row['group_kirim_seq'];          
			
			//cari temp sesuai groupnya
			$sql = 	"SELECT sql_1, sql_2, sql_3, group_kirim_seq, jumlah_group, urutan_group ".
					"FROM dml_temp WHERE group_kirim_seq = $seq ORDER BY group_kirim_seq, urutan_group ASC";
			$stmt2 = $db->prepare($sql);
			$stmt2->execute();
			$array2 = $stmt2->fetchAll();

			

			//execute sqlnya
			foreach($array2 as $row2) { 
				$sql_1 = $row2['sql_1']; 
				$sql_2 = $row2['sql_2'];
				$sql_3 = $row2['sql_3'];
				$sql_execute = $sql_1;

				if (strpos(strtoupper($sql_1), "MASTER_BARANG") !== false){
					$JalankanUpdate = true;
				}

				if($sql_2 != ''){
					$stmt2 = $db->prepare($sql_2); 
					$cek = $stmt2->execute();
					if ($cek) {
						if ($stmt2->rowCount() > 0) {							
							$sql_execute = $sql_3;
						}else{
							$sql_execute = $sql_1;
						}					
					} 						
				}				

				$stmtakhir = $db->prepare($sql_execute);                                   
                if (!$stmtakhir->execute()){
                    $db->rollBack();
                    return $response->withJson(["status" => "gagal"], 100);  
                }
			} 
			
			//delete temp 
			$sql_1 = "DELETE FROM dml_temp WHERE group_kirim_seq = $seq";
			$stmtakhir = $db->prepare($sql_1);
			$stmtakhir->execute();
			if (!$stmtakhir->execute()){
				$db->rollBack();
				return $response->withJson(["status" => "gagal"], 100);  
			}

			//Update klasifikasi jika ada perubahan ke master barang
			if ($JalankanUpdate == true){
				$sql = 	"UPDATE master_barang SET klasifikasi = 'Diamond' ". 
				"WHERE (DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(tgl_release) AND DATE(DATE_ADD(tgl_release, INTERVAL 3 MONTH)) OR (DATE(tgl_release) > DATE(DATE_FORMAT(NOW(),'%y-%m-%d'))))";
				$query = $db->prepare($sql);
				$query->execute();

				$sql = 	"UPDATE master_barang SET klasifikasi = 'Sapphire' ".
						"WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(DATE_ADD(DATE_ADD(tgl_release, INTERVAL 3 MONTH), INTERVAL 1 DAY)) AND DATE(DATE_ADD(tgl_release, INTERVAL 12 MONTH))";
				$query = $db->prepare($sql);
				$query->execute();

				$sql = 	"UPDATE master_barang SET klasifikasi = 'Emerald' ".
						"WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(DATE_ADD(DATE_ADD(tgl_release, INTERVAL 12 MONTH), INTERVAL 1 DAY)) AND DATE(DATE_ADD(tgl_release, INTERVAL 24 MONTH))";
				$query = $db->prepare($sql);
				$query->execute();

				$sql = 	"UPDATE master_barang SET klasifikasi = 'Rubi' ".
						"WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) > DATE(DATE_ADD(DATE_ADD(tgl_release, INTERVAL 24 MONTH), INTERVAL 1 DAY))";
				$query = $db->prepare($sql);
				$query->execute();
			}
      	} 
      	$db->commit();  

    } catch(PDOException $pdoe) {		
        $db->rollBack();
		// throw $pdoe;
        $sql2 = "INSERT INTO DML_TEMP_ERROR(sql_1, sql_2, sql_3, group_kirim_seq, jumlah_group, urutan_group) SELECT sql_1, sql_2, sql_3, group_kirim_seq, jumlah_group, urutan_group FROM dml_temp WHERE group_kirim_seq = $seq";
        $stmt3 = $db->prepare($sql2);
        $stmt3->execute();

        //delete temp 
        $sql2 = "delete FROM dml_temp WHERE group_kirim_seq = $seq";
        $stmtakhir = $db->prepare($sql2);
        $stmtakhir->execute();
                
        return $response->withJson(["status" => "gagal",
                                    "error execute "=>$pdoe
                                   , "sql_1" => $sql_1
                                   , "sql_2" => $sql_2
                                   , "sql_3" => $sql_3
                                    ], 200);   
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal".$e], 200);  
    }
    return $response->withJson(["status" => "success"], 200);
})->add($cekAPIKey);


$app->get('/dml/cek_dml_error', function (Request $request, Response $response) {
    $db = $this->db;
    $sql = "SELECT * FROM DML_TEMP_ERROR";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    if($stmt->rowCount() == 0){
      return $response->withJson(["status" => "tidak ada dml gagal lagi"], 200);  
    }else{
      return $response->withJson(["status" => "terdapat data gagal"], 200);
    }
});