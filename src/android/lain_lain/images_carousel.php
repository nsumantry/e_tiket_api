<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Digunakan untuk mendapatkan daftar gambar carousel
$app->get('/images_carousel/load', function (Request $request, Response $response) {		
	$query = $this->db->prepare("SELECT seq, path FROM images_carousel");								 						
	$result = $query->execute();
	
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});