<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/versi_app/get', function (Request $request, Response $response, array $args) {
    $versi = $request->getQueryParam("versi");
    
    $sql =  "SELECT tanggal, versi, versi_name FROM versi_program ";
    $qry = $this->db->prepare($sql);
    $qry->execute();
    $arr = $qry->fetch();
    
    $versiApp = $arr["versi"];
    if ($versiApp > $versi){                
        return $response->withJson(["status" => "harus update"]);
    }else{
        return $response->withJson(["status" => "terupdate"]);
    }
});