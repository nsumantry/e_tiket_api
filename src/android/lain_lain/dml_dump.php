<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/dml_dump/load', function (Request $request, Response $response, array $args) {	 	
	$seq = $request->getQueryParam("seq"); 	
	$sql =  "SELECT seq, sql_1, is_execute ".			
    	   "FROM dml_dump m WHERE m.seq > $seq";
  	$query = $this->db->prepare($sql);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array();
		}
	}else{
			$data = array();
	}
  	return $response->withJson($data);
});