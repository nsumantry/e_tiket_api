<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Digunakan untuk mendapatkan daftar gambar size pack
$app->get('/size_pack/get', function (Request $request, Response $response) {		
	$sql = "SELECT IFNULL(seq,0) AS seq , path FROM images_size_pack";	

	$query = $this->db->prepare($sql);		
	$result = $query->execute();
	
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});